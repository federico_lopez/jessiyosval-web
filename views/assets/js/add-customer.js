/*------------------------------------------Agregar Cliente-----------------------------------------------------------*/
$('form[name="insert-customer"]').on('submit', function(e){
    e.preventDefault();
    var pet = $(this).attr('action');
    var met = $(this).attr('method');
    var customer = $('input[name="name"]').val().toUpperCase();
    // alert(pet+met+provider);
    $.ajax({
        beforeSend: function(){

        },
        url : pet,
        type: met,
        data: $(this).serialize(),
        success: function(resp){
            if(resp > 0){
                //guardado();
                $('#select-customer').append($('<option>', {
                    value: resp,
                    text: customer
                })).val(resp).change();
                $('#new-customer').modal('toggle');
                $('input[name="name"]').val('');
                $('input[name="document"]').val('');
                $('input[name="birthdate"]').val('');
                $('input[name="phone"]').val('');
                $('input[name="address"]').val('');
                $('input[name="email"]').val('');
                $('input[name="rode"]').focus();
            }else{
                alert('Error: el cliente ya existe!!')
            }
            console.log(resp);
        },
        error: function (jqXHR,stado,error){
            console.log(estado);
            console.log(error);
        }
    });
});
