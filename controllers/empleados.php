<?php
/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 5/3/2018
 * Time: 11:43
 */

/*---------------------------------------------------------------Index Empleado----------------------------------------*/
$app->get('/empleados', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Empleados.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $employed = new Empleados();
        $employes = $employed->selectEmployed();

        $app->render('empleado/empleado.html.twig', array(
            'employees' => $employes, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('empleado');

/*--------------------------------------Insertar Empleado--------------------------------------------------------------*/
$app->post('/empleado/nuevo', function() use($app){

    require_once 'models/Empleados.php';

    $employed = new Empleados();
    $request = $app->request;

    $employed->setNombre(strtoupper($request->post('name')));
    $employed->setRuc($request->post('ruc'));
    $employed->setCelular($request->post('phone'));
    $employed->setCorreo($request->post('email'));


    $fecha=date_create($request->post('birthdate'));
    $date = date_format($fecha,"Y/m/d");
    $employed->setNacimiento($date);

    $employed->setEstado('A');
    $employed->setIdRol(3);
    $employed->setPin('1234');
    $employed->setFoto('employed.png');

    $insert = $employed->insertEmployed();

    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Empleado insertado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Empleado duplicado o error de datos!!');
    }

    $app->redirect($app->urlFor('empleado'));


})->name('insert-employed');

/*----------------------------------------------------Actualizar Empleado----------------------------------------------*/
$app->get('/update/empleado/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Empleados.php';
        require_once 'models/Selectores.php';

        $empleado = new Empleados();
        $empleado->setIdEmpleado($id);
        $result = $empleado->selectEmployedId();

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $app->render('empleado/update-employed.html.twig', array(
            'user' => $userAr, 'employed' => $result));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('update-employed-page');

$app->post('/empleado/update', function() use($app){

    require_once 'models/Empleados.php';
    $employed = new Empleados();
    $request = $app->request;

    $employed->setIdempleado($request->post('id'));
    $employed->setNombre(strtoupper($request->post('name')));
    $employed->setRuc($request->post('ruc'));
    $employed->setCelular($request->post('phone'));
    $employed->setCorreo($request->post('email'));

    $fecha=date_create($request->post('birthdate'));
    $date = date_format($fecha,"Y/m/d");
    $employed->setNacimiento($date);

    $update = $employed->updateEmployed();
    if($update){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Editado Correctamente');
    }

    $app->redirect($app->urlFor('empleado'));

})->name('update-employed');

/*---------------------------------------------Desactivar Empleado-----------------------------------------------------*/
$app->get('/empleado-update/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Empleados.php';

        $employed = new Empleados();
        $employed->setIdEmpleado($id);
        $employed->setEstado('I');
        $eliminado = $employed->deleteEmployed();

        if($eliminado){
            $app->flash('content', 'alert-success');
            $app->flash('mensaje', 'Empleado eliminado correctamente!!');
        }

        $app->redirect($app->urlFor('empleado'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('delete-employed');

/*---------------------------------------------ELIMINAR EMPLEADO------------------------------------------------------*/
$app->get('/empleado/delete/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Empleados.php';

        $employed = new Empleados();
        $employed->setIdEmpleado($id);
        $eliminado = $employed->deleteDefinitiveEmployed();

        if($eliminado){
            $app->flash('content', 'alert-success');
            $app->flash('mensaje', 'Empleado eliminado correctamente!!');
        }

        $app->redirect($app->urlFor('empleado'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('delete-definitive-employed');