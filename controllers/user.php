<?php
/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 24/08/2016
 * Time: 03:18 PM
 */

/*----------Index Usuarios----------*/
$app->get('/users', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/User.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $usuario = new User();
        $usuarios = $usuario->verUser();

        $app->render('user/user.html.twig', array(
            'usuarios' => $usuarios, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('usuarios');



/*----------Registrar Usuario-----------*/
$app->post('/user/register', function() use($app){

    require_once 'models/User.php';
    $registro = new User();
    $request = $app->request;

    $registro->setUsuario(strtolower($request->post('usuario')));
    $pass = $request->post('pass');
    $repeatpass = $request->post('pass2');
    $registro->setNombreApellido(strtoupper($request->post('nombre')));
    $registro->setEmail(strtolower($request->post('email')));
    $registro->setIdRol($request->post('rol'));
    $registro->setEstado($request->post('estado'));
    $registro->setEstadoUsuario('1');

    //Naciiento usuario
    $nacimiento=$request->post('nacimiento');
    if(!empty($nacimiento)){
        $date=date_create($nacimiento);
        $registro->setNacimiento(date_format($date,"Y-m-d"));
    }

    if($registro->getIdRol()=="3"){
        $registro->setPhoto("admin.png");
    }else{
        $registro->setPhoto("https://cdn3.iconfinder.com/data/icons/users-6/100/654853-user-men-2-256.png");
    }


    if($pass == $repeatpass){
        $secret = "geoaccidente";
        $passEncript = hash_hmac("sha512",$pass,$secret);
        $registro->setPass($passEncript);

         $registro->insertarUser();

        //inserta Rol
        $maxid = $registro->selectMaxId();
        $registro->setIdUsuario($maxid["max"]);
        $registro->insertarRol();

        $app->flash('content', 'alert-success');
        $app->flash('mensaje',  'Usuario creado Correctamente!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Contraseñas Incorrectas');
    }

    //echo "/".$registro->getNombreApellido()."/".$registro->getUsuario()."/".$registro->getNacimiento()."/".$registro->getEmail()."/".$registro->getIdRol()."/".$registro->getEstado()."/".$registro->getPass();

    $app->redirect($app->urlFor('usuarios'));

})->name('new-user');

/*----------Buscar Pacientes----------*/
$app->post('/busqueda-usuario', function() use($app){

    require_once 'models/Selectores.php';
    $request = $app->request;
    $nombre = strtolower($request->post('buscar'));

    $selector = new Selectores();
    $userAr = $selector->returnRol();

    $sentencia = "SELECT u.id_usuario, u.usuario,u.email,u.nombre_apellido,r.estado,r.id_rol, e.rol, e.estado estado_rol
                  FROM usuarios u, usuario_rol r, roles e
                  WHERE u.id_usuario = r.id_usuario and r.id_rol = e.id_rol and lower(u.nombre_apellido) LIKE '%$nombre%'
                  ORDER BY u.nombre_apellido";
    $usuarioAr = $selector->sentenciaAll($sentencia);

    $app->render('user/user.html.twig', array('usuarios' => $usuarioAr, 'user' => $userAr));

})->name('search-users');

/*----------Modificar Usuario----------*/
$app->get('/update/user/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/User.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $usuarios = new User();
        $usuarios->setIdUsuario($id);
        $usuario = $usuarios->selectUser();

        $app->render('user/update-user.html.twig', array( 'usuario' => $usuario,  'user' => $userAr));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('up-usuario');


$app->post('/user/update', function() use($app){

    require_once 'models/User.php';
    $usuario = new User();
    $request = $app->request;

    $usuario->setIdUsuario($request->post('id'));
    $usuario->setUsuario(strtolower($request->post('usuario')));
    $usuario->setNombreApellido(strtoupper($request->post('nombre')));
    $usuario->setEmail(strtolower($request->post('email')));
    $usuario->setEstado($request->post('estado'));
    $nacimiento=$request->post('nacimiento');
    if(!empty($nacimiento)){
        $date=date_create($nacimiento);
        $usuario->setNacimiento(date_format($date,"Y-m-d"));
    }
    //echo $pass."/".$repeatpass."/".$passEncript;
    $resul = $usuario->updateUser();
    if($resul>0){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Usuario Actualizado Correctamente');
    }else {
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido actualizar el Usuario');
    }
    //echo $usuario->getIdUsuario()."/".$usuario->getUsuario()."/".$usuario->getNombreApellido()."/".$usuario->getNacimiento()."/".$usuario->getEmail()."/".$usuario->getEstado();

    $app->redirect($app->urlFor('usuarios'));

})->name('update-user');


/*-----------------------------------------Index Pefil------------------------------------------------------------------*/
$app->get('/perfil', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/User.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $usuario = new User();
        $usuario->setIdUsuario($userAr['iduser']);
        $user = $usuario->selectUser();

        $app->render('user/profile.html.twig', array(
            'usuario' => $user, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('profile');

/*-------------------------------------------------------ACTUALIZAR PASSWORD--------------------------------------------------*/
$app->post('/user/update-password', function() use($app){

    require_once 'models/User.php';
    $registro = new User();
    $request = $app->request;

    $app->flash('icon','fa-exclamation-triangle');
    $app->flash('content-icon', 'alert-danger');
    $app->flash('message-icon', 'Las contraseñas no coinciden');

    $registro->setIdUsuario($request->post('id-user'));
    $passPrevious = $request->post('password');
    $passNew = $request->post('pass');
    $passRep = $request->post('pass-rep');

    if($passNew == $passRep){
        $usuario = $registro->selectUser();
        $passwordPrevious = $usuario['pass'];

        $secret = "geoaccidente";
        $passPreviousEncript = hash_hmac("sha512",$passPrevious,$secret);

        if($passPreviousEncript==$passwordPrevious){
            $passNewEncript = hash_hmac("sha512",$passNew,$secret);
            $registro->setPass($passNewEncript);
            $registro->updatePassword();

            $app->flash('icon','fa-check');
            $app->flash('content-icon', 'alert-success');
            $app->flash('message-icon', 'Contraseña actualizada correctamente');
        }else{
            $app->flash('message-icon', 'Contraseña anterior incorrecta');
        }

    }

    $app->redirect($app->urlFor('profile'));

})->name('update-password');

/*-------------------------------------------------------ACTUALIZAR PERFIL--------------------------------------------------*/
$app->post('/user/update-profile', function() use($app){

    require_once 'models/User.php';
    $registro = new User();
    $request = $app->request;

    $app->flash('icon','fa-exclamation-triangle');
    $app->flash('content-icon', 'alert-danger');
    $app->flash('message-icon', 'No se pudo modificar');

    $registro->setIdUsuario($request->post('id-user'));
    $registro->setUsuario($request->post('usuario'));
    $registro->setEmail($request->post('email'));
    $registro->setNombreApellido($request->post('nombre'));
    $registro->setEstado('A');

    $nacimiento=$request->post('nacimiento');
    $date=date_create($nacimiento);
    $registro->setNacimiento(date_format($date,"Y-m-d"));

    $actualizado=$registro->updateUser();

    if ($actualizado) {
        $app->flash('icon', 'fa-check');
        $app->flash('content-icon', 'alert-success');
        $app->flash('message-icon', 'Datos actualizados correctamente');
    }

    $app->redirect($app->urlFor('profile'));

})->name('update-profile');

/*-----------------------------------------Actualizar Foto Perfil---------------------------------------------------------*/
$app->post('/profile/update-image', function() use($app){
    require_once 'models/User.php';
    $user = new User();
    $request = $app->request;

    $user->setIdUsuario($request->post('id-user'));
    $lastId = $user->getIdUsuario();

    $image = getimagesize($_FILES['image']['tmp_name']);
    $mime = $image['mime'];
    if ($mime != 'image/gif' and $mime != 'image/png' and $mime != 'image/jpeg') {
        $app->flash('icon','fa-exclamation-triangle');
        $app->flash('content-icon', 'alert-danger');
        $app->flash('message-icon', 'El tipo de archivo seleccionado no es una imagen');

    } else {
        //OBTENGO EL NOMBRE Y LA RUTA DE LA IMAGEN
        $image_name = $_FILES['image']['name'];
        $image_tmp_directory = $_FILES['image']['tmp_name'];
        //DIRECTORIO DONDE SE GUADARAN LAS IMAGENES
        $dir = "views/assets/images/profile/";

        if ($mime === 'image/png') {
            $imagen_original = imagecreatefrompng($image_tmp_directory);
            $name_img = ".png";
            $name_img_small = "small.png";
        } elseif ($mime === 'image/jpeg') {
            $imagen_original = imagecreatefromjpeg($image_tmp_directory);
            $name_img = ".jpeg";
            $name_img_small = "small.jpeg";
        } elseif ($mime === 'image/gif') {
            $imagen_original = imagecreatefromgif($image_tmp_directory);
            $name_img = ".gif";
            $name_img_small = "small.gif";
        }

        //Obtengo el ancho y  alto de la imagen que cargue
        $ancho_original = imagesx($imagen_original);
        $alto_original = imagesy($imagen_original);
        //Redimensiono
        $width_small = 100;
        $height_small = 100;
        $width = 305;
        $height = 305;
        $imagen_resized = imagecreatetruecolor($width, $height);
        $imagen_resized_small = imagecreatetruecolor($width_small, $height_small);
        imagecopyresampled($imagen_resized, $imagen_original, 0, 0, 0, 0, $width, $height, $ancho_original, $alto_original);
        imagecopyresampled($imagen_resized_small, $imagen_original, 0, 0, 0, 0, $width_small, $height_small, $ancho_original, $alto_original);


        $name_img = strtr($name_img, " ", "-");//reemplazo espacios por guion medio
        $name_img = strtr($name_img, "/", "-");//reemplazo barra diagonal por guion medio
        $name_img = strtolower($name_img);//convierto a minusculas

        $name_img_small = strtr($name_img_small, " ", "-");//reemplazo espacios por guion medio
        $name_img_small = strtr($name_img_small, "/", "-");//reemplazo barra diagonal por guion medio
        $name_img_small = strtolower($name_img_small);//convierto a minusculas

        $image = $dir . $lastId . "-" . $name_img;
        $image_small = $dir . $lastId . "-" . $name_img_small;

        $nameImage = $lastId . "-" . $name_img;
        $nameImageSmall = $lastId . "-" . $name_img_small;
        /* $arrayName = array(
             "small" => $nameImageSmall,
             "big" => $nameImage
         );*/

        if ($mime === 'image/png') {
            imagepng($imagen_resized, $image);
            imagepng($imagen_resized_small, $image_small);
        } elseif ($mime === 'image/jpeg') {
            imagejpeg($imagen_resized, $image);
            imagejpeg($imagen_resized_small, $image_small);
        } elseif ($mime === 'image/gif') {
            imagegif($imagen_resized, $image);
            imagegif($imagen_resized_small, $image_small);
        }

        imagedestroy($imagen_original);
        imagedestroy($imagen_resized);
        imagedestroy($imagen_resized_small);

        //$product->setImgSmall($nameImageSmall);
        $user->setPhoto($nameImage);
        $user->updateImageProfile();

        $app->flash('icon','fa-check');
        $app->flash('content-icon', 'alert-success');
        $app->flash('message-icon', 'Foto actualizada correctamente');
    }

    $app->redirect($app->urlFor('profile'));

})->name('update-img-profile');


/*------------------------------------------RESET PASSWORD------------------------------------------------------------*/
$app->get('/user/reset-password/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){

        require_once 'models/User.php';
        $registro = new User();
    
        $registro->setIdUsuario($id);
        $registro->setPass("1238db1b6e69a57c904c0ba315c07e2ee5dfa4acef7883876a836046f89a9d37af1d836905182820f05850f25e618eddfd9f7306f4800215fb0f688077f9482b");
        $result=$registro->updatePassword();
        
        if($result){
            $content = "Contraseña Reseteada!";
            $app->flash('content', 'alert-success');
            $app->flash('mensaje', $content);
        }else {
            $content = "No se ha podido Resetear la contraseña!";
            $app->flash('mensaje', $content);
            $app->flash('content', 'alert-danger');
        }
    
        $app->redirect($app->urlFor('usuarios'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('reset-pass');