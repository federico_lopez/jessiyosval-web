<?php

$app->get('/login', function() use($app){
    //redireccionar si hay session
    if(!empty($_SESSION['session'])){

        $app->redirect($app->urlFor('dashboard'));
    }else{
        //si no hay session dibujar la pagina de login
        $app->render('login.html.twig');
    }
})->name('login');

/*----------Form Login-----------*/
$app->post('/login', function() use($app){

    require_once 'models/Login.php';
    $login = new login();
    $request = $app->request;

    $login->setUser(intval(preg_replace('/[^0-9]+/','', $request->post('user'))));
    $pin = $request->post('pin');
    $login->setPin($pin);
    $login->consulta();
    $arrayUser = $login->getResult();
    //echo $login->getPin().$login->getUser();
    if($login->check() == true){
        //si el login fue exitoso, ingresar redireccionando
        $app->redirect($app->urlFor('customer'));

    }else{
        //si no volver a la pagina de login con un mensaje flash
        $app->flash('error', 'Usuario o contraseña incorrecta');
        $app->redirect($app->urlFor('login'));
    }
})->name('login-form');


/*----------Cerrar Sesion-----------*/
$app->get('/logout', function() use($app){

    //Borro las cookies de session para no dejar ningun rastro
    //Nota: Esto destruira la session y no la informacion de la session!!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }

    //Finalmente destruyo la session
    session_destroy();
    //Redirect al login
    $app->redirect($app->urlFor('login'));

})->name('logout');