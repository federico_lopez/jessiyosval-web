<?php
/**
 * Created by PhpStorm.
 * User: Servitec
 * Date: 04/04/2019
 * Time: 17:44
 */
/*-------------------------------------------------Registrar tarjetas-------------------------------------------------*/
$app->get('/registrar-tarjetas', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Tarjetas.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $customer = $selector->sentenciaAll("SELECT id_cliente, nombre FROM CLIENTE");

        $card = new Tarjetas();
        $result = $card->selectTarjetas();

        $app->render('tarjeta/register-card.html.twig', array(
            'customer' => $customer, 'cards'=>$result, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('card-register');

/*------------------------------------------------INSERTAR TARJETAS---------------------------------------------------*/
$app->post('/insertar-tarjeta', function() use($app){

    require_once 'models/Tarjetas.php';

    $card = new Tarjetas();
    $request = $app->request;

    $card->setIdCliente($request->post('customer'));
    $card->setIdTarjeta($request->post('number'));
    $card->setPuntos(0);

    //echo $card->getIdTarjeta()."/".$card->getIdCliente();
    $insert = $card->insertCard();

    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Tarjeta insertada correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Error de datos!!');
    }

    $app->redirect($app->urlFor('card-register'));

})->name('card-insert');

/*-------------------------------------------------CARGAR PUNTOS-------------------------------------------------*/
$app->get('/carga-de-puntos', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $app->render('tarjeta/add-point.html.twig', array(
           'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('card-add-point');

/*-------------------------------------------------Ver Puntos Acumulados----------------------------------------------*/
$app->get('/customer-vip/:id', function($id) use($app){
    require_once 'models/Tarjetas.php';

    $card = new Tarjetas();
    $card->setIdTarjeta($id);
    $result = $card->selectPoints();

    $app->render('tarjeta/point.html.twig', array(
        'customer' => $result));

})->name('point');