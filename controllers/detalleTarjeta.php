<?php
/**
 * Created by PhpStorm.
 * User: Servitec
 * Date: 06/04/2019
 * Time: 16:41
 */
/*------------------------------------------------INSERTAR TARJETAS---------------------------------------------------*/
$app->post('/insertar-puntos', function() use($app){

    require_once 'models/DetalleTarjeta.php';
    require_once 'models/Tarjetas.php';
    date_default_timezone_set("America/Asuncion");

    $card = new Tarjetas();
    $detail = new DetalleTarjeta();

    $request = $app->request;

    $detail->setIdTarjeta($request->post('number'));
    $detail->setPunto($request->post('point'));

    $date = date('Y-m-d');
    $detail->setFecha($date);
    $insertPoint = $detail->insertDetailCard();

    if($insertPoint){
        $card->setIdTarjeta($detail->getIdTarjeta());
        $resultPoint=$card->selectPoints();
        $point=$resultPoint["puntos"]+$detail->getPunto();
        $card->setPuntos($point);
        $card->updatePoint();

        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Puntos cargados correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Error de datos!!');
    }

    //echo $point;
    $app->redirect($app->urlFor('card-register'));

})->name('card-insert-point');