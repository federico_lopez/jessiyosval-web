<?php
/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 9/3/2018
 * Time: 13:13
 */

/*---------------------------------------------Index Clientes---------------------------------------------------------*/
$app->get('/clientes', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $customer = new Cliente();
        $result = $customer->selectCustomer();

        $app->render('cliente/customer.html.twig', array(
            'customer' => $result, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('customer');

/*------------------------------------Busqueda de  Clientes por Nombre------------------------------------------------*/
$app->get('/clientes/:nombre', function($name) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $customer = new Cliente();
        $customer->setNombre(strtolower($name));
        $result = $customer->selectCustomerName();

        $app->render('cliente/customer.html.twig', array(
            'customer' => $result, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('customer-search');

/*--------------------------------Insertar Cliente AJAX---------------------------------------------------------------*/
$app->post('/cliente/nuevo-ajax', function() use($app){

    require_once 'models/Cliente.php';
    $customer = new Cliente();
    $request = $app->request;

    $customer->setNombre(strtoupper($request->post('name')));
    $customer->setDireccion($request->post('address'));
    $customer->setRuc($request->post('ruc'));
    $customer->setCelular($request->post('phone'));
    $customer->setCorreo($request->post('email'));
    $customer->setDocumento($request->post('document'));

    $fecha=date_create($request->post('birthdate'));
    $date = date_format($fecha,"Y/m/d");
    $customer->setNacimiento($date);

    $insertado=$customer->insertCustomer();
    echo $insertado;

})->name('insert-customer-ajax');

/*--------------------------------------Insertar Cliente--------------------------------------------------------------*/
$app->post('/cliente/nuevo', function() use($app){

    require_once 'models/Cliente.php';

    $customer = new Cliente();
    $request = $app->request;

    $customer->setNombre(strtoupper($request->post('name')));
    $customer->setDocumento($request->post('documento'));
    $customer->setCelular($request->post('phone'));
    $customer->setCorreo($request->post('email'));
    $customer->setDireccion($request->post('address'));

    $fecha=date_create($request->post('birthdate'));
    $date = date_format($fecha,"Y/m/d");
    $customer->setNacimiento($date);

    $insert = $customer->insertCustomer();

    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Cliente insertado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Cliente duplicado o error de datos!!');
    }

    $app->redirect($app->urlFor('customer'));


})->name('insert-customer');

/*---------------------------------------------ELIMINAR CLIENTE-------------------------------------------------------*/
$app->get('/customer/delete/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';

        $customer = new Cliente();
        $customer->setIdCliente($id);
        $eliminado = $customer->deleteDefinitiveCustomer();

        if($eliminado){
            $app->flash('mensaje', 'Empleado eliminado correctamente!!');
        }else{
            $customer->setEstado("I");
            $customer->inactiveCustomer();
            $app->flash('mensaje', 'Empleado desactivado correctamente!!');
        }
        $app->flash('content', 'alert-success');
        $app->redirect($app->urlFor('customer'));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('delete-customer');

/*----------------------------------------------------Actualizar Empleado----------------------------------------------*/
$app->get('/update/customer/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';
        require_once 'models/Selectores.php';

        $customer = new Cliente();
        $customer->setIdCliente($id);
        $result = $customer->selectCustomerId();

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $app->render('cliente/update-customer.html.twig', array(
            'user' => $userAr, 'customer' => $result));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('update-customer-page');

$app->post('/customer/update', function() use($app){

    require_once 'models/Cliente.php';
    $customer = new Cliente();
    $request = $app->request;

    $customer->setIdCliente($request->post('id'));
    $customer->setNombre(strtoupper($request->post('name')));
    $customer->setRuc($request->post('ruc'));
    $customer->setCelular($request->post('phone'));
    $customer->setCorreo($request->post('email'));
    $customer->setDireccion($request->post('city'));

    $fecha=date_create($request->post('birthdate'));
    $date = date_format($fecha,"Y/m/d");
    $customer->setNacimiento($date);

    $update = $customer->updateCustomer();
    if($update){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Editado Correctamente');
    }

    $app->redirect($app->urlFor('customer'));

})->name('update-customer');

