<?php
/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 9/3/2018
 * Time: 12:57
 */

class Cliente{

    private $idCliente;
    private $ruc;
    private $nombre;
    private $nacimiento;
    private $direccion;
    private $celular;
    private $correo;
    private $documento;
    private $estado;

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * @param mixed $idCliente
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

    /**
     * @return mixed
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param mixed $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    /**
     * @param mixed $nacimiento
     */
    public function setNacimiento($nacimiento)
    {
        $this->nacimiento = $nacimiento;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param mixed $correo
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return mixed
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @param mixed $documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }


    /**
     * @result todos los clientes con 100 de limite
     */
    public function selectCustomer(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM cliente WHERE estado='A' order by id_cliente DESC LIMIT 100 ");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    #result busqueda de cliente por nombre
    public function selectCustomerName(){
        $customer= "%".$this->getNombre()."%";
        $sentencia = "SELECT * FROM cliente WHERE lower(nombre) LIKE :nombre";
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute(array('nombre' => $customer));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * Insert cliente nuevo
     */
    public function insertCustomer(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO cliente(nombre,ruc,nacimiento, direccion, celular, correo, documento) VALUES (:nombre,:ruc,:nacimiento, :direccion, :celular, :correo, :documento);");
        $insert=$query->execute(array('nombre' => $this->getNombre(),
            'ruc' => $this->getRuc(),
            'nacimiento' => $this->getNacimiento(),
            'direccion' => $this->getDireccion(),
            'celular' => $this->getCelular(),
            'correo' => $this->getCorreo(),
            'documento' => $this->getDocumento(),
        ));

        $id=0;
        if($insert){
            $id = $conexion->lastInsertId();
        }

        $conexion = null;
        return $id;
    }

    #elimina un cliente por id
    public function deleteDefinitiveCustomer(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM cliente
                                    WHERE id_cliente = :id;");
        $query->execute(array('id' => $this->getIdCliente()));
        $conexion = null;
        return $query->rowCount();
    }

    #desactiva un cliente, cambia estado de A a I
    public function inactiveCustomer(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE cliente
                                    SET estado = :estado
                                    WHERE id_cliente = :id;");
        $query->execute(array('estado' => $this->getEstado(),
            'id' => $this->getIdCliente()));
        $conexion = null;
        return $query->rowCount();
    }

    #selecciona un cliente por su id
    public function selectCustomerId(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM cliente WHERE id_cliente=:id");
        $query->execute(array('id' => $this->getIdCliente()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    #actualiza los datos de un cliente por su id
     public function updateCustomer(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE cliente
                                    SET nombre = :nombre, ruc = :ruc, nacimiento = :nacimiento, celular = :celular,
                                    correo = :email, direccion = :ciudad
                                    WHERE id_cliente = :id;");
        $query->execute(array('nombre' => $this->getNombre(),
            'ruc' => $this->getRuc(),
            'nacimiento' => $this->getNacimiento(),
            'celular' => $this->getCelular(),
            'email' => $this->getCorreo(),
            'ciudad' => $this->getDireccion(),
            'id' => $this->getIdCliente()));
        $conexion = null;
        return $query->rowCount();
    }






}

