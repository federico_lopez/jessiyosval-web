<?php
/**
 * Created by PhpStorm.
 * User: Servitec
 * Date: 06/04/2019
 * Time: 16:41
 */

class DetalleTarjeta{
    private $idDetalleTarjeta;
    private $fecha;
    private $idTarjeta;
    private $punto;

    /**
     * @return mixed
     */
    public function getIdDetalleTarjeta()
    {
        return $this->idDetalleTarjeta;
    }

    /**
     * @param mixed $idDetalleTarjeta
     */
    public function setIdDetalleTarjeta($idDetalleTarjeta)
    {
        $this->idDetalleTarjeta = $idDetalleTarjeta;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getIdTarjeta()
    {
        return $this->idTarjeta;
    }

    /**
     * @param mixed $idTarjeta
     */
    public function setIdTarjeta($idTarjeta)
    {
        $this->idTarjeta = $idTarjeta;
    }

    /**
     * @return mixed
     */
    public function getPunto()
    {
        return $this->punto;
    }

    /**
     * @param mixed $punto
     */
    public function setPunto($punto)
    {
        $this->punto = $punto;
    }

    #Inserta un historial de los puntos cargados
    public function insertDetailCard(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO detalle_tarjeta(fecha, id_tarjeta, punto) VALUES (:fecha, :id, :puntos);");
        $query->execute(array('fecha' => $this->getFecha(),
            'id' => $this->getIdTarjeta(),
            'puntos' => $this->getPunto()
        ));
        $conexion = null;
        return $query->rowCount();
    }



}