<?php
/**
 * Created by PhpStorm.
 * User: Servitec
 * Date: 04/04/2019
 * Time: 17:44
 */

class Tarjetas{

    private $idTarjeta;
    private $puntos;
    private $idCliente;

    /**
     * @return mixed
     */
    public function getIdTarjeta()
    {
        return $this->idTarjeta;
    }

    /**
     * @param mixed $idTarjeta
     */
    public function setIdTarjeta($idTarjeta)
    {
        $this->idTarjeta = $idTarjeta;
    }

    /**
     * @return mixed
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * @param mixed $puntos
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * @param mixed $idCliente
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

    #return todas las tarjetas registradas
    public function selectTarjetas(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT t.id_tarjeta, t.puntos, c.nombre
                                                FROM tarjetas t, cliente c
                                                WHERE t.id_cliente=c.id_cliente;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    #Inserta las tarjetas, asigna por numero a cliente
    public function insertCard(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO tarjetas(id_tarjeta, id_cliente, puntos) VALUES (:id, :cliente, :puntos);");
        $query->execute(array('id' => $this->getIdTarjeta(),
            'cliente' => $this->getIdCliente(),
            'puntos' => $this->getPuntos()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    #return los puntos acumulados de acuerdo al idTarjeta
    public function selectPoints(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT t.id_tarjeta, t.puntos, c.nombre
                                              FROM tarjetas t, cliente c
                                              WHERE t.id_cliente=c.id_cliente AND id_tarjeta=:id;");
        $query->execute(array('id' => $this->getIdTarjeta()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }


    #actualiza los puntos de la tarjeta
    public function updatePoint(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE tarjetas
                                    SET puntos = :puntos
                                    WHERE id_tarjeta = :id;");
        $query->execute(array('puntos' => $this->getPuntos(),
            'id' => $this->getIdTarjeta()));
        $conexion = null;
        return $query->rowCount();
    }

}