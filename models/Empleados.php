<?php
/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 5/3/2018
 * Time: 11:35
 */

class Empleados{
    private $idEmpleado;
    private $nombre;
    private $ruc;
    private $nacimiento;
    private $celular;
    private $correo;
    private $foto;
    private $contrasena;
    private $pin;
    private $estado;
    private $id_rol;

    /**
     * @return mixed
     */
    public function getIdEmpleado()
    {
        return $this->idEmpleado;
    }

    /**
     * @param mixed $idEmpleado
     */
    public function setIdEmpleado($idEmpleado)
    {
        $this->idEmpleado = $idEmpleado;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param mixed $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }

    /**
     * @return mixed
     */
    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    /**
     * @param mixed $nacimiento
     */
    public function setNacimiento($nacimiento)
    {
        $this->nacimiento = $nacimiento;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param mixed $correo
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * @return mixed
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * @param mixed $contrasena
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }

    /**
     * @return mixed
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * @param mixed $pin
     */
    public function setPin($pin)
    {
        $this->pin = $pin;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->id_rol;
    }

    /**
     * @param mixed $id_rol
     */
    public function setIdRol($id_rol)
    {
        $this->id_rol = $id_rol;
    }


    public function selectEmployed(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM empleado WHERE estado='A' order by nombre ");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    public function insertEmployed(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO empleado(nombre, ruc, nacimiento, celular, correo, foto, pin, estado, id_rol) VALUES (:nombre, :ruc, :nacimiento, :celular, :correo, :foto, :pin, :estado, :id_rol);");
        $query->execute(array('nombre' => $this->getNombre(),
            'ruc' => $this->getRuc(),
            'nacimiento' => $this->getNacimiento(),
            'celular' => $this->getCelular(),
            'correo' => $this->getCorreo(),
            'foto' => $this->getFoto(),
            'pin' => $this->getPin(),
            'estado' => $this->getEstado(),
            'id_rol' => $this->getIdRol(),
        ));
        $conexion = null;
        return $query->rowCount();
    }

    public function selectEmployedId(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM empleado WHERE id_empleado=:id order by nombre ");
        $query->execute(array('id' => $this->getIdEmpleado()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    public function deleteEmployed(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE empleado
                                    SET estado = :estado
                                    WHERE id_empleado = :id;");
        $query->execute(array('estado' => $this->getEstado(),
            'id' => $this->getIdEmpleado()));
        $conexion = null;
        return $query->rowCount();
    }

    public function updateEmployed(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE empleado
                                    SET nombre = :nombre, ruc = :ruc, nacimiento = :nacimiento, celular = :celular,
                                    correo = :email
                                    WHERE id_empleado = :id;");
        $query->execute(array('nombre' => $this->getNombre(),
            'ruc' => $this->getRuc(),
            'nacimiento' => $this->getNacimiento(),
            'celular' => $this->getCelular(),
            'email' => $this->getCorreo(),
            'id' => $this->getIdEmpleado()));
        $conexion = null;
        return $query->rowCount();
    }

    public function deleteDefinitiveEmployed(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM empleado
                                    WHERE id_empleado = :id;");
        $query->execute(array('id' => $this->getIdEmpleado()));
        $conexion = null;
        return $query->rowCount();
    }


}