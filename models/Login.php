<?php
class login{

    public $user;
    public $pass;
    public $result;
    public $pin;

    /**
     * @return mixed
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * @param mixed $pin
     */
    public function setPin($pin)
    {
        $this->pin = $pin;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    public function consulta(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_empleado,nombre, ruc, pin, id_rol, correo, foto, nacimiento
                                    FROM empleado
                                    WHERE id_empleado=:usuario AND pin = :pass");
        $query->execute(array('usuario' => $this->getUser(), 'pass' => $this->getPin()));
        $this->result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
    }

    public function check(){
        if(!empty($this->result)){
            $_SESSION['session'] = $this->result;
            return true;
        }else{
            return false;
        }
    }

}
