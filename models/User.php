<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 24/08/2016
 * Time: 03:25 PM
 */
class User{

    private $id_usuario;
    private $usuario;
    private $email;
    private $nombre_apellido;
    private $estado;
    private $id_rol;
    private $nacimiento;
    private $pass;
    private $estadoUsuario;
    private $photo;

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getEstadoUsuario()
    {
        return $this->estadoUsuario;
    }

    /**
     * @param mixed $estadoUsuario
     */
    public function setEstadoUsuario($estadoUsuario)
    {
        $this->estadoUsuario = $estadoUsuario;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    /**
     * @param mixed $nacimiento
     */
    public function setNacimiento($nacimiento)
    {
        $this->nacimiento = $nacimiento;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNombreApellido()
    {
        return $this->nombre_apellido;
    }

    /**
     * @param mixed $nombre_apellido
     */
    public function setNombreApellido($nombre_apellido)
    {
        $this->nombre_apellido = $nombre_apellido;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->id_rol;
    }

    /**
     * @param mixed $id_rol
     */
    public function setIdRol($id_rol)
    {
        $this->id_rol = $id_rol;
    }



    public function verUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT u.id_usuario, u.usuario,u.email,u.nombre_apellido,u.estado,r.id_rol, e.rol, r.estado estado_rol, u.photo
                                    FROM usuarios u, usuario_rol r, roles e
                                    WHERE u.id_usuario = r.id_usuario and r.id_rol = e.id_rol
                                    ORDER BY u.id_usuario DESC LIMIT 100;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO usuarios(usuario, pass, email, nombre_apellido, estado, fecha_nacimiento, estado_usuario, photo)
                                    VALUES (:usuario, :pass, :email, :nombre, :estado, :nacimiento, :estado_user, :photo);");
        $query->execute(array('usuario' => $this->getUsuario(),
            'pass' => $this->getPass(),
            'email' => $this->getEmail(),
            'nombre' => $this->getNombreApellido(),
            'estado' => $this->getEstado(),
            'nacimiento' => $this->getNacimiento(),
            'estado_user' => $this->getEstadoUsuario(),
            'photo'=>$this->getPhoto()));
        $conexion = null;
    }

    public function insertarRol(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO usuario_rol(id_usuario, id_rol, estado)
                                    VALUES (:idUser, :idRol, :estado);");
        $query->execute(array('idUser' => $this->getIdUsuario(),
            'idRol' => $this->getIdRol(),
            'estado' => $this->getEstado()));
        $conexion = null;
    }

    public function selectMaxId(){
        $conexion = new Conexion();
        $query = $conexion->prepare("select max(id_usuario) from usuarios");
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function selectUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("select * from usuarios WHERE id_usuario=:id");
        $query->execute(array('id' => $this->getIdUsuario()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function updateUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE usuarios SET usuario=:usuario, nombre_apellido=:nombre, fecha_nacimiento=:nacimiento, estado=:estado, email=:email WHERE id_usuario=:id;");
        $query->execute(array('usuario' => $this->getUsuario(),
            'nombre' => $this->getNombreApellido(),
            'nacimiento' => $this->getNacimiento(),
            'estado' => $this->getEstado(),
            'email' => $this->getEmail(),
            'id' => $this->getIdUsuario()));
        $conexion = null;
        return $query->rowCount();
    }

    public function updateImageProfile(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE usuarios SET photo=:photo WHERE id_usuario=:id;");
        $query->execute(array('photo' => $this->getPhoto(),
            'id' => $this->getIdUsuario()));
        $conexion = null;
        return $query->rowCount();
    }

    public function updatePassword(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE usuarios SET pass=:pass WHERE id_usuario=:id;");
        $query->execute(array('pass' => $this->getPass(),
            'id' => $this->getIdUsuario()));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * @return la posicion y estado de los inspectores para ubicar en el mapa
     */
    public function selectInspectores(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT u.id_usuario, u.usuario,u.nombre_apellido,r.id_rol, u.estado_usuario, u.photo, u.latitud, u.longitud, u.estado
                                    FROM usuarios u, usuario_rol r
                                    WHERE u.id_usuario = r.id_usuario AND id_rol=2 AND u.estado='A'
                                    ORDER BY u.id_usuario;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }
}