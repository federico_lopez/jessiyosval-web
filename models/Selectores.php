<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 03/05/2016
 * Time: 01:12 PM
 */
class Selectores
{

    public function returnRol(){
        $userAr['idempleado'] = $_SESSION['session']['id_empleado'];
        $userAr['idrol'] = $_SESSION['session']['id_rol'];
        $userAr['nombre'] = $_SESSION['session']['nombre'];
        $userAr['nacimiento'] = $_SESSION['session']['nacimiento'];
        $userAr['email'] = $_SESSION['session']['correo'];
        $userAr['foto'] = $_SESSION['session']['foto'];
        return $userAr;
    }

    public function sentenciaAll($sentencia){
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    public function sentencia($sentencia){
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }


}