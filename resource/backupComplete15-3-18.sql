--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

-- Started on 2018-03-15 14:31:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2173 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 16394)
-- Name: boleta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE boleta (
    id_boleta integer NOT NULL,
    numero character varying(10),
    fecha date,
    total integer,
    id_cliente integer,
    estado character varying(1) DEFAULT 'A'::character varying
);


ALTER TABLE boleta OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16397)
-- Name: boleta_id_boleta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE boleta_id_boleta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE boleta_id_boleta_seq OWNER TO postgres;

--
-- TOC entry 2174 (class 0 OID 0)
-- Dependencies: 182
-- Name: boleta_id_boleta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE boleta_id_boleta_seq OWNED BY boleta.id_boleta;


--
-- TOC entry 183 (class 1259 OID 16399)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cliente (
    id_cliente integer NOT NULL,
    ruc character varying(20),
    nombre character varying(150),
    nacimiento date,
    direccion character(250),
    celular character varying(50),
    correo character varying(100),
    documento integer
);


ALTER TABLE cliente OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16405)
-- Name: cliente_id_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cliente_id_cliente_seq
    START WITH 792
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cliente_id_cliente_seq OWNER TO postgres;

--
-- TOC entry 2175 (class 0 OID 0)
-- Dependencies: 184
-- Name: cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cliente_id_cliente_seq OWNED BY cliente.id_cliente;


--
-- TOC entry 185 (class 1259 OID 16407)
-- Name: detalle_boleta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE detalle_boleta (
    id_detalle_boleta integer NOT NULL,
    costo integer,
    porcentaje integer,
    cantidad_porcentaje integer,
    id_boleta integer,
    id_empleado integer,
    id_tratamiento integer
);


ALTER TABLE detalle_boleta OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16410)
-- Name: detalle_boleta_id_detalle_boleta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE detalle_boleta_id_detalle_boleta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detalle_boleta_id_detalle_boleta_seq OWNER TO postgres;

--
-- TOC entry 2176 (class 0 OID 0)
-- Dependencies: 186
-- Name: detalle_boleta_id_detalle_boleta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE detalle_boleta_id_detalle_boleta_seq OWNED BY detalle_boleta.id_detalle_boleta;


--
-- TOC entry 187 (class 1259 OID 16412)
-- Name: empleado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE empleado (
    id_empleado integer NOT NULL,
    nombre character varying(150) NOT NULL,
    ruc character varying(20),
    nacimiento date,
    celular character varying(50),
    correo character varying(80),
    foto character varying(100),
    contrasena character varying(15),
    pin integer,
    estado character varying(1) NOT NULL,
    id_rol integer
);


ALTER TABLE empleado OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16415)
-- Name: empleado_id_empleado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empleado_id_empleado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE empleado_id_empleado_seq OWNER TO postgres;

--
-- TOC entry 2177 (class 0 OID 0)
-- Dependencies: 188
-- Name: empleado_id_empleado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empleado_id_empleado_seq OWNED BY empleado.id_empleado;


--
-- TOC entry 189 (class 1259 OID 16417)
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE rol (
    id_rol integer NOT NULL,
    rol character varying(15)
);


ALTER TABLE rol OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16420)
-- Name: rol_idrol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rol_idrol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rol_idrol_seq OWNER TO postgres;

--
-- TOC entry 2178 (class 0 OID 0)
-- Dependencies: 190
-- Name: rol_idrol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE rol_idrol_seq OWNED BY rol.id_rol;


--
-- TOC entry 191 (class 1259 OID 16422)
-- Name: tratamiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tratamiento (
    id_tratamiento integer NOT NULL,
    tratamiento character varying(50),
    costo integer,
    porcentaje integer
);


ALTER TABLE tratamiento OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16425)
-- Name: tratamiento_id_tratamiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tratamiento_id_tratamiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tratamiento_id_tratamiento_seq OWNER TO postgres;

--
-- TOC entry 2179 (class 0 OID 0)
-- Dependencies: 192
-- Name: tratamiento_id_tratamiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tratamiento_id_tratamiento_seq OWNED BY tratamiento.id_tratamiento;


--
-- TOC entry 2012 (class 2604 OID 16427)
-- Name: id_boleta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boleta ALTER COLUMN id_boleta SET DEFAULT nextval('boleta_id_boleta_seq'::regclass);


--
-- TOC entry 2014 (class 2604 OID 16428)
-- Name: id_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente ALTER COLUMN id_cliente SET DEFAULT nextval('cliente_id_cliente_seq'::regclass);


--
-- TOC entry 2015 (class 2604 OID 16429)
-- Name: id_detalle_boleta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_boleta ALTER COLUMN id_detalle_boleta SET DEFAULT nextval('detalle_boleta_id_detalle_boleta_seq'::regclass);


--
-- TOC entry 2016 (class 2604 OID 16430)
-- Name: id_empleado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado ALTER COLUMN id_empleado SET DEFAULT nextval('empleado_id_empleado_seq'::regclass);


--
-- TOC entry 2017 (class 2604 OID 16431)
-- Name: id_rol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rol ALTER COLUMN id_rol SET DEFAULT nextval('rol_idrol_seq'::regclass);


--
-- TOC entry 2018 (class 2604 OID 16432)
-- Name: id_tratamiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tratamiento ALTER COLUMN id_tratamiento SET DEFAULT nextval('tratamiento_id_tratamiento_seq'::regclass);


--
-- TOC entry 2154 (class 0 OID 16394)
-- Dependencies: 181
-- Data for Name: boleta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY boleta (id_boleta, numero, fecha, total, id_cliente, estado) FROM stdin;
3	000125	2018-03-14	\N	788	A
4	000126	2018-03-14	\N	775	A
1	001234	2018-03-13	85000	1	A
\.


--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 182
-- Name: boleta_id_boleta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('boleta_id_boleta_seq', 5, true);


--
-- TOC entry 2156 (class 0 OID 16399)
-- Dependencies: 183
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cliente (id_cliente, ruc, nombre, nacimiento, direccion, celular, correo, documento) FROM stdin;
2		ADELA	2018-01-06	                                                                                                                                                                                                                                                          			\N
3		ADRIANA	2015-03-30	                                                                                                                                                                                                                                                          			\N
4		AGUSTINA RECALDE	1998-04-16	                                                                                                                                                                                                                                                          	295654		\N
5		AIDA ESPINOLA	2016-10-02	                                                                                                                                                                                                                                                          			\N
6		AIDA GIMENEZ	2015-12-30	                                                                                                                                                                                                                                                          			\N
7		AIXA	2016-06-04	                                                                                                                                                                                                                                                          			\N
8		ALBA	1987-02-17	                                                                                                                                                                                                                                                          			\N
9		ALBA FRANCO	2016-10-08	                                                                                                                                                                                                                                                          			\N
10		ALBERTO TORRES	2016-12-04	                                                                                                                                                                                                                                                          			\N
11		ALCIRA	1998-02-15	                                                                                                                                                                                                                                                          			\N
12		ALDO	1975-01-26	                                                                                                                                                                                                                                                          			\N
13		ALEJANDRA	2016-11-06	                                                                                                                                                                                                                                                          			\N
14		ALEJANDRA GARAY	2015-11-07	                                                                                                                                                                                                                                                          			\N
15		ALEJANDRA HERRERA	2016-08-12	                                                                                                                                                                                                                                                          			\N
16		ALELI	2016-06-04	                                                                                                                                                                                                                                                          			\N
17		ALEXANDRA	2015-12-19	                                                                                                                                                                                                                                                          			\N
18		ALICE ORTIZ	2017-03-05	                                                                                                                                                                                                                                                          			\N
19		ALICIA	2015-12-22	                                                                                                                                                                                                                                                          			\N
20		ALICIA DE VILLALBA	1974-04-09	                                                                                                                                                                                                                                                          			\N
21		ALICIA GAUTO	2017-02-05	                                                                                                                                                                                                                                                          			\N
22		ALICIA MOYANO	2015-09-06	                                                                                                                                                                                                                                                          	0981505396		\N
23		ALMA ALVEIRO	2017-12-16	                                                                                                                                                                                                                                                          			\N
24		ALQUILER	2015-12-21	                                                                                                                                                                                                                                                          			\N
25		AMPARO SANCHEZ	2017-12-20	                                                                                                                                                                                                                                                          			\N
26		AMY ARRUA	2017-02-05	                                                                                                                                                                                                                                                          			\N
27		ANA	2015-12-11	                                                                                                                                                                                                                                                          			\N
28		ANA BELEN DE BRIX PEREZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
29		ANA CARDOZO	2016-12-11	                                                                                                                                                                                                                                                          			\N
30		ANA ESPINOLA	2016-12-04	                                                                                                                                                                                                                                                          			\N
31		ANA FANEGO	2017-08-12	                                                                                                                                                                                                                                                          			\N
32		ANA GONZALEZ	2016-09-01	                                                                                                                                                                                                                                                          			\N
33		ANA LAURA AYALA	2016-01-15	                                                                                                                                                                                                                                                          			\N
34		ANA LIZ	2015-12-21	                                                                                                                                                                                                                                                          			\N
35		ANA MARIA	2015-12-02	                                                                                                                                                                                                                                                          			\N
36		ANA MARIA GUERRA	2016-11-13	                                                                                                                                                                                                                                                          			\N
37		ANA MARIA ZUBIZARRETA	2016-12-04	                                                                                                                                                                                                                                                          			\N
38		ANA MAYOR	2016-11-06	                                                                                                                                                                                                                                                          			\N
39		ANA OJEDA	2016-08-10	                                                                                                                                                                                                                                                          			\N
40		ANA PAULA	1997-06-15	                                                                                                                                                                                                                                                          			\N
41		ANA PEREZ	2015-07-26	                                                                                                                                                                                                                                                          	0982915151		\N
42		ANA SOSA	2017-06-09	                                                                                                                                                                                                                                                          			\N
43		ANABEL	2015-12-28	                                                                                                                                                                                                                                                          			\N
44		ANAHI	2015-11-13	                                                                                                                                                                                                                                                          			\N
46		ANAHI CHAMORRO	2016-11-06	                                                                                                                                                                                                                                                          			\N
47		ANALI GONZALEZ	2015-12-15	                                                                                                                                                                                                                                                          			\N
48		ANALIA DIAZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
49		ANALIA LEDESMA	2016-08-12	                                                                                                                                                                                                                                                          			\N
50		ANALIA MEZA	2016-11-06	                                                                                                                                                                                                                                                          			\N
51		ANALIA SALGUEIRO	2016-10-01	                                                                                                                                                                                                                                                          			\N
52		ANALIZ ACOSTA	2016-11-06	                                                                                                                                                                                                                                                          			\N
53		ANCHI	2017-05-07	                                                                                                                                                                                                                                                          			\N
54		ANDREA	2015-12-22	                                                                                                                                                                                                                                                          			\N
55		ANDREA (PRODUCCION)	2016-12-11	                                                                                                                                                                                                                                                          			\N
56		ANDREA CAMPOS ANGULO	1990-04-09	                                                                                                                                                                                                                                                          			\N
57		ANDREA LOPEZ	2016-10-01	                                                                                                                                                                                                                                                          			\N
58		ANDREA MAIDANA	2016-10-07	                                                                                                                                                                                                                                                          			\N
59		ANDREA VEGA	2017-01-01	                                                                                                                                                                                                                                                          			\N
60		ANDREA ZARZA	2015-12-20	                                                                                                                                                                                                                                                          	0981953080		\N
61		ANDRESA	2017-05-26	                                                                                                                                                                                                                                                          			\N
62		ANGELA	1984-05-24	                                                                                                                                                                                                                                                          			\N
63		ANGELA VILLALBA	2016-12-11	                                                                                                                                                                                                                                                          			\N
64		ANGELES	2015-12-08	                                                                                                                                                                                                                                                          			\N
65		ANGELI PENZZI	2016-11-06	                                                                                                                                                                                                                                                          			\N
66		ANGELICA	2017-08-31	                                                                                                                                                                                                                                                          			\N
67		ANGELICA ROA	1984-01-20	                                                                                                                                                                                                                                                          	0992246859		\N
68		ANGELINA	2016-06-02	                                                                                                                                                                                                                                                          			\N
69		ANGI	2015-11-17	                                                                                                                                                                                                                                                          			\N
70		ANGI  ESPINZI	1998-04-13	                                                                                                                                                                                                                                                          	0984456231		\N
71		ANNY ALMEIDA	2017-12-30	                                                                                                                                                                                                                                                          			\N
72		ANTONELLA	2015-03-30	                                                                                                                                                                                                                                                          			\N
73		ANTONIA	1998-05-15	                                                                                                                                                                                                                                                          			\N
74		ANTONY ARRUA	1990-11-04	                                                                                                                                                                                                                                                          			\N
75		ARA CHAMORRO	1990-04-16	                                                                                                                                                                                                                                                          			\N
76		ARACELI	2015-11-14	                                                                                                                                                                                                                                                          			\N
77		ARAMI	2016-01-02	                                                                                                                                                                                                                                                          			\N
78		ARIANA	1987-02-25	                                                                                                                                                                                                                                                          			\N
79		ARIANNE PERDOMO	2016-10-02	                                                                                                                                                                                                                                                          			\N
80		ASUNCION DOMINGUEZ	2017-02-12	                                                                                                                                                                                                                                                          			\N
81		AURORA	2017-03-05	                                                                                                                                                                                                                                                          			\N
82		AXEL	1998-09-25	                                                                                                                                                                                                                                                          			\N
83		AYESA FRUTOS	2016-09-01	                                                                                                                                                                                                                                                          			\N
84		BARBARA	2015-12-29	                                                                                                                                                                                                                                                          			\N
85		BARBARA PANIAGUA	2016-11-05	                                                                                                                                                                                                                                                          			\N
86		BASILIA MARECOS	2017-02-05	                                                                                                                                                                                                                                                          			\N
87		BEATRIZ	2015-12-14	                                                                                                                                                                                                                                                          			\N
88		BELEN	1978-04-15	                                                                                                                                                                                                                                                          			\N
89		BELEN LEIVA	2016-11-06	                                                                                                                                                                                                                                                          			\N
90		BELEN SANGUINA..	1998-11-10	                                                                                                                                                                                                                                                          			\N
91		BERENICE	2016-05-13	                                                                                                                                                                                                                                                          			\N
92		BETANIA	2016-12-04	                                                                                                                                                                                                                                                          			\N
93		BETY	2016-01-07	                                                                                                                                                                                                                                                          			\N
94		BLANCA	2016-10-08	                                                                                                                                                                                                                                                          			\N
95		BLANCA BRITEZ	2017-01-01	                                                                                                                                                                                                                                                          			\N
96		BLANCA BRIZUELA	2016-12-04	                                                                                                                                                                                                                                                          			\N
97		BLANCA ORTIZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
98		BLANCA VARELA	2018-01-26	                                                                                                                                                                                                                                                          			\N
99		CABALLERO	2015-11-04	                                                                                                                                                                                                                                                          			\N
100		CAMILA	2015-12-30	                                                                                                                                                                                                                                                          			\N
101		CARINA	1978-02-02	                                                                                                                                                                                                                                                          			\N
102		CARINA AGUILAR	1981-05-08	                                                                                                                                                                                                                                                          			\N
103		CARINA FLOR	2015-12-28	                                                                                                                                                                                                                                                          			\N
104		CARLA	2017-01-08	                                                                                                                                                                                                                                                          			\N
105		CARMEN	2015-12-22	                                                                                                                                                                                                                                                          			\N
106		CARMEN GONZALEZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
107		CARMEN PERALTA	2016-10-01	                                                                                                                                                                                                                                                          			\N
108		CARMINHA	2015-11-20	                                                                                                                                                                                                                                                          			\N
109		CAROL	2016-01-05	                                                                                                                                                                                                                                                          			\N
110		CAROL GONZALEZ	1985-07-30	                                                                                                                                                                                                                                                          	0982449681		\N
111		CAROLINA	1978-02-15	                                                                                                                                                                                                                                                          			\N
112		CAROLINA RAMIREZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
113		CAROLINA RODRIGUEZ	1979-10-10	                                                                                                                                                                                                                                                          	0983216735		\N
114		CAROLINA SOTELO	2017-03-05	                                                                                                                                                                                                                                                          			\N
115		CASILDA TRINIDAD	0017-05-07	                                                                                                                                                                                                                                                          			\N
116		CATALINA	1978-01-29	                                                                                                                                                                                                                                                          			\N
117		CECI	2015-12-12	                                                                                                                                                                                                                                                          			\N
118		CECIL SCHNEIDER	2017-02-05	                                                                                                                                                                                                                                                          			\N
119		CECILIA	2016-10-02	                                                                                                                                                                                                                                                          			\N
120		CELESTE GOMEZ	1984-05-24	                                                                                                                                                                                                                                                          	0982600752		\N
121		CELIA ARANA	2017-02-04	                                                                                                                                                                                                                                                          			\N
122		CELIA	2015-12-08	                                                                                                                                                                                                                                                          			\N
123		CELSA	2016-01-02	                                                                                                                                                                                                                                                          			\N
124		CELSA ARGUELLO	2016-12-04	                                                                                                                                                                                                                                                          			\N
125		CESAR CARDOZO	2016-09-08	                                                                                                                                                                                                                                                          			\N
126		CINDY FRANCO	1990-07-25	                                                                                                                                                                                                                                                          			\N
127		CINTHIA	2015-11-07	                                                                                                                                                                                                                                                          			\N
128		CINTHIA PEREIRA	1986-01-01	                                                                                                                                                                                                                                                          	0991843330		\N
129		CINTHIA ZELAYA	1998-04-05	                                                                                                                                                                                                                                                          	0981773679		\N
130		CLARA	2015-12-08	                                                                                                                                                                                                                                                          			\N
131		CLARISE ARRIOLA	1979-07-31	                                                                                                                                                                                                                                                          	0972242338		\N
132		CLARISSE PORTILLO	2016-10-08	                                                                                                                                                                                                                                                          			\N
133		CLARIZZA MEZA	2017-01-08	                                                                                                                                                                                                                                                          			\N
134		CLAUDIA	2015-11-30	                                                                                                                                                                                                                                                          			\N
135		CLAUDIA ALLEGRO	1985-07-30	                                                                                                                                                                                                                                                          			\N
136		CLAUDIA BRAGA	2016-11-06	                                                                                                                                                                                                                                                          			\N
137		CLAUDIA CABRERA	2016-10-07	                                                                                                                                                                                                                                                          			\N
138		CLAUDIA MAYOR	2017-02-05	                                                                                                                                                                                                                                                          			\N
139		CLAUDIA PORTILLO	2016-07-06	                                                                                                                                                                                                                                                          			\N
140		CLAUDINA	1970-03-01	                                                                                                                                                                                                                                                          			\N
141		CLAUDINA RAMIREZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
142		CLOTILDE RIVEROS	2015-11-24	                                                                                                                                                                                                                                                          			\N
143		CONCEPCION GONZALEZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
144		CONTANZA MUNHOZ	2017-01-01	                                                                                                                                                                                                                                                          			\N
145		CORINA OJEDA	2017-02-04	                                                                                                                                                                                                                                                          			\N
146		CRISTHIAN	2016-10-01	                                                                                                                                                                                                                                                          			\N
147		CRISTINA VARGAS	2016-01-23	                                                                                                                                                                                                                                                          	0985758304		\N
148		CYNDI FRANCO	1985-07-30	                                                                                                                                                                                                                                                          			\N
149		CYNTHIA	2016-10-01	                                                                                                                                                                                                                                                          			\N
150		CYNTHIA ADORNO	2016-12-04	                                                                                                                                                                                                                                                          			\N
151		CYNTHIA DIAZ	2016-10-01	                                                                                                                                                                                                                                                          			\N
152		CYNTHIA PEREIRA	2015-11-07	                                                                                                                                                                                                                                                          			\N
153		CYNTHIA ROA	0012-05-08	                                                                                                                                                                                                                                                          			\N
154		CYNTHIA ROJAS	2015-12-11	                                                                                                                                                                                                                                                          			\N
155		CYNTHIA SAUCEDO	2016-10-02	                                                                                                                                                                                                                                                          			\N
156		CYNTHIA URAM	2015-12-30	                                                                                                                                                                                                                                                          			\N
157		CYNTHIA VERA	2016-12-18	                                                                                                                                                                                                                                                          			\N
158		DAHIANA	2017-08-05	                                                                                                                                                                                                                                                          			\N
159		DAHIANA GUARIE	2015-11-06	                                                                                                                                                                                                                                                          	0983218795		\N
160		DALILA	2017-01-01	                                                                                                                                                                                                                                                          			\N
161		DANA	2016-10-01	                                                                                                                                                                                                                                                          			\N
162		DANIA MORENO	2015-03-25	                                                                                                                                                                                                                                                          	0981411679		\N
163		DANIEL	2016-11-06	                                                                                                                                                                                                                                                          			\N
164		DANILA	1998-04-15	                                                                                                                                                                                                                                                          			\N
165		DANNA	2015-12-30	                                                                                                                                                                                                                                                          			\N
166		DANNA MARTINEZ	1983-09-21	                                                                                                                                                                                                                                                          	0982937734		\N
167		DARA	2015-12-08	                                                                                                                                                                                                                                                          			\N
168		DAYSI	2015-12-26	                                                                                                                                                                                                                                                          			\N
169		DEBORA	2017-08-30	                                                                                                                                                                                                                                                          			\N
170		DEIMA	2016-10-06	                                                                                                                                                                                                                                                          			\N
171		DELIA GUERRERO	2016-07-08	                                                                                                                                                                                                                                                          			\N
172		DELIA.	2015-11-18	                                                                                                                                                                                                                                                          			\N
173		DELSY	2017-04-02	                                                                                                                                                                                                                                                          			\N
174		DENIS NUNHEZ	2016-10-05	                                                                                                                                                                                                                                                          			\N
175		DENISSE ROJAS	2015-11-05	                                                                                                                                                                                                                                                          			\N
176		DEYMA PRIETO	2017-02-04	                                                                                                                                                                                                                                                          			\N
177		DIANA	2016-01-05	                                                                                                                                                                                                                                                          			\N
178	3423894-8	DIANA ALARCON	1985-04-24	                                                                                                                                                                                                                                                          			\N
179		DIANA DAVALOS	2016-11-06	                                                                                                                                                                                                                                                          			\N
180		DIANA MARINHO	1983-01-27	                                                                                                                                                                                                                                                          	0985714125		\N
181		DIANA RAMIREZ	2016-05-05	                                                                                                                                                                                                                                                          			\N
182		DIANA VAZQUEZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
183		DILA	2017-09-16	                                                                                                                                                                                                                                                          			\N
184		DINA DE ALARCON	1958-10-22	                                                                                                                                                                                                                                                          			\N
185		DINA ROMAN	2016-10-14	                                                                                                                                                                                                                                                          			\N
186		DOLLY PORTILLO	2017-06-04	                                                                                                                                                                                                                                                          			\N
187		DORA	2015-12-19	                                                                                                                                                                                                                                                          			\N
188		DULCE	2017-09-29	                                                                                                                                                                                                                                                          			\N
189		EDITH	2016-07-09	                                                                                                                                                                                                                                                          			\N
190		EDUARDO FARINHA	2016-10-06	                                                                                                                                                                                                                                                          			\N
191		EDUARDO MERELES	2015-11-10	                                                                                                                                                                                                                                                          			\N
192		ELENA	2015-12-12	                                                                                                                                                                                                                                                          			\N
193		ELENA BRIZUELA	2016-12-10	                                                                                                                                                                                                                                                          			\N
194		ELENA DOMINGUEZ	2017-02-05	                                                                                                                                                                                                                                                          			\N
195		ELENA GONZALEZ	2016-10-02	                                                                                                                                                                                                                                                          			\N
196		ELENITA	2015-11-03	                                                                                                                                                                                                                                                          			\N
197		ELGA	2016-11-06	                                                                                                                                                                                                                                                          			\N
198		ELI	2016-07-06	                                                                                                                                                                                                                                                          			\N
199		ELIODORA	2016-10-02	                                                                                                                                                                                                                                                          			\N
200		ELISA	2015-12-12	                                                                                                                                                                                                                                                          			\N
201		ELISA FLEITAS	2015-01-03	                                                                                                                                                                                                                                                          	0983293837		\N
202		ELISABEL	1998-04-15	                                                                                                                                                                                                                                                          			\N
203		ELIZABETH	2016-10-02	                                                                                                                                                                                                                                                          			\N
204		ELIZABETH DE MARTINEZ	2016-11-13	                                                                                                                                                                                                                                                          			\N
205		ELVIA SANABRIA	2015-12-14	                                                                                                                                                                                                                                                          	0983597569		\N
206		ELVIA..	2015-11-14	                                                                                                                                                                                                                                                          			\N
207		ELVIRA	2016-11-06	                                                                                                                                                                                                                                                          			\N
208		EMI SALDIVAR	1985-03-11	                                                                                                                                                                                                                                                          	0981698529	emysaldivar@outlook.es	\N
209		EMI STELATTO	2105-05-30	                                                                                                                                                                                                                                                          	0981553670		\N
210		EMILCE DELVALLE	2017-10-30	                                                                                                                                                                                                                                                          			\N
211		EMILIA	2015-12-29	                                                                                                                                                                                                                                                          			\N
212		ENRIQUE	2016-10-02	                                                                                                                                                                                                                                                          			\N
213		ERICA	2015-03-30	                                                                                                                                                                                                                                                          			\N
214		ERIKA ARCE	2016-12-04	                                                                                                                                                                                                                                                          			\N
215		ERIKA HOGE	2017-02-05	                                                                                                                                                                                                                                                          			\N
216		ERMELINA	2017-11-04	                                                                                                                                                                                                                                                          			\N
217		ESMERALDA	2015-12-21	                                                                                                                                                                                                                                                          			\N
218		ESPERANZA	2015-11-27	                                                                                                                                                                                                                                                          			\N
219		ESPERANZA GODOY	2017-02-05	                                                                                                                                                                                                                                                          			\N
220		ESTEFANIA	2017-05-24	                                                                                                                                                                                                                                                          			\N
221		ESTELA	2015-12-19	                                                                                                                                                                                                                                                          			\N
222		ESTELA CRUZ	2016-09-01	                                                                                                                                                                                                                                                          			\N
223		ESTELA PEDROZO	2016-12-11	                                                                                                                                                                                                                                                          			\N
224		ESTER	2017-03-12	                                                                                                                                                                                                                                                          			\N
225		ESTER MENDEZ	2016-12-03	                                                                                                                                                                                                                                                          			\N
226		EVA BENITEZ	2016-08-12	                                                                                                                                                                                                                                                          			\N
227		EVA GONZALEZ	2015-11-27	                                                                                                                                                                                                                                                          			\N
228		EVELIN	2016-10-06	                                                                                                                                                                                                                                                          			\N
229		EVELIN (REVISTA)	2016-10-01	                                                                                                                                                                                                                                                          			\N
230		EVELIN ARCA	2017-02-04	                                                                                                                                                                                                                                                          			\N
231		EVELIN PERALTA	2016-12-11	                                                                                                                                                                                                                                                          			\N
232		EVELYN LEIVA	2015-12-10	                                                                                                                                                                                                                                                          			\N
233		FABI	2015-12-14	                                                                                                                                                                                                                                                          			\N
234		FABIOLA	2015-12-17	                                                                                                                                                                                                                                                          			\N
235		FABIOLA TEXEIRA	2015-12-08	                                                                                                                                                                                                                                                          			\N
236		FANI SANCHEZ	2016-10-01	                                                                                                                                                                                                                                                          			\N
237		FANNY	2016-05-07	                                                                                                                                                                                                                                                          			\N
238		FATIMA	2015-12-14	                                                                                                                                                                                                                                                          			\N
239		FATIMA AVALOS	2015-11-12	                                                                                                                                                                                                                                                          			\N
240		FATIMA FRETES	0080-01-27	                                                                                                                                                                                                                                                          	0981104292		\N
241		FELIX GIMENEZ	2016-04-15	                                                                                                                                                                                                                                                          			\N
242		FELIX RAMOS	1974-04-15	                                                                                                                                                                                                                                                          			\N
243		FERNANDA	2016-12-04	                                                                                                                                                                                                                                                          			\N
244		FERNANDO	2016-10-06	                                                                                                                                                                                                                                                          			\N
245		FERNANDO CONTRERAS	2016-06-17	                                                                                                                                                                                                                                                          			\N
246		FIORELLA	2015-12-14	                                                                                                                                                                                                                                                          			\N
247		FLORENCIA	2016-06-02	                                                                                                                                                                                                                                                          			\N
248		FLOSTILDA	2016-10-02	                                                                                                                                                                                                                                                          			\N
249		FRANCISCA	1998-05-15	                                                                                                                                                                                                                                                          			\N
250		GABI	2015-11-14	                                                                                                                                                                                                                                                          			\N
251		GABRIEL	2016-09-03	                                                                                                                                                                                                                                                          			\N
252		GABRIELA	1995-08-15	                                                                                                                                                                                                                                                          			\N
253		GABRIELA OLAZAR	2016-06-04	                                                                                                                                                                                                                                                          			\N
254		GABY	2016-11-06	                                                                                                                                                                                                                                                          			\N
255		GANADORES DE RADIO	2015-11-19	                                                                                                                                                                                                                                                          			\N
256		GELEN	1993-04-05	                                                                                                                                                                                                                                                          			\N
257		GERALDINE	2018-01-06	                                                                                                                                                                                                                                                          			\N
258	3436879-3	GERDA GOSLING	2017-02-05	                                                                                                                                                                                                                                                          			\N
259		GISELL	1958-04-16	                                                                                                                                                                                                                                                          			\N
260		GLADYS	1989-01-30	                                                                                                                                                                                                                                                          			\N
261		GLADYS NOGUERA	1963-03-27	                                                                                                                                                                                                                                                          	0971507863		\N
262		GLORIA	2016-10-06	                                                                                                                                                                                                                                                          			\N
263		GLORIA (TUN TUN)	2016-10-02	                                                                                                                                                                                                                                                          			\N
264		GLORIA DEL PUERTO	2015-03-28	                                                                                                                                                                                                                                                          			\N
265		GLORIA DEL VALLE	2017-07-14	                                                                                                                                                                                                                                                          			\N
266		GLORIA ESTIGARRIBIA	2017-02-05	                                                                                                                                                                                                                                                          			\N
267		GLORIA GONZALEZ	0020-09-18	                                                                                                                                                                                                                                                          			\N
269		GLORIA GRANADO	0019-02-06	                                                                                                                                                                                                                                                          			\N
270		GLORIA HELMAN	2017-02-05	                                                                                                                                                                                                                                                          			\N
271		GLORIA MARECOS	2016-11-06	                                                                                                                                                                                                                                                          			\N
272		GLORIA MARTINEZ	1998-01-15	                                                                                                                                                                                                                                                          			\N
273		GLORIA MEZA	2016-11-13	                                                                                                                                                                                                                                                          			\N
274		GLORIA SESSA	1974-05-02	                                                                                                                                                                                                                                                          			\N
275		GLORIA VILLALBA	1992-07-12	                                                                                                                                                                                                                                                          	0984567576		\N
276		GLORIA. CREDI AGIL	0013-05-07	                                                                                                                                                                                                                                                          	0972282537		\N
277		GRACIELA	2015-12-28	                                                                                                                                                                                                                                                          			\N
278		GRACIELA GARCIA	1986-11-16	                                                                                                                                                                                                                                                          	0961101554		\N
279		GRISELDA BRACHO	0020-01-06	                                                                                                                                                                                                                                                          			\N
280		GRISELDA SANCHEZ	2016-10-14	                                                                                                                                                                                                                                                          			\N
281		GRISELDA SOSA	2017-03-04	                                                                                                                                                                                                                                                          			\N
282		GRISSIA	2017-10-10	                                                                                                                                                                                                                                                          			\N
283		GUADALUPE GUILLEN	2017-02-04	                                                                                                                                                                                                                                                          			\N
284		GUADALUPE ORTEGA	1987-06-14	                                                                                                                                                                                                                                                          	0994883631		\N
285		GUILLERMINA	2017-06-10	                                                                                                                                                                                                                                                          			\N
286	488496-5	GUSTAVO MARTINEZ	2017-03-05	                                                                                                                                                                                                                                                          			\N
287		HEIDE	2016-11-06	                                                                                                                                                                                                                                                          			\N
288		HELENA	2016-11-06	                                                                                                                                                                                                                                                          			\N
289	691302-4	HELMUT SIEMENS	0026-11-06	MAYOR VERA 355- SAN LORENZO                                                                                                                                                                                                                               			\N
290		HERMELINDA	2017-03-05	                                                                                                                                                                                                                                                          			\N
291		HERMINIA	2015-12-24	                                                                                                                                                                                                                                                          			\N
292		HILDA 	2017-09-01	                                                                                                                                                                                                                                                          			\N
293		HORTENSIA	2016-09-07	                                                                                                                                                                                                                                                          			\N
294		HUGO	2016-05-14	                                                                                                                                                                                                                                                          			\N
295		HUGO VEGA	2016-09-15	                                                                                                                                                                                                                                                          			\N
296		HUMBERTO	2016-06-04	                                                                                                                                                                                                                                                          			\N
297	875.098-0	ILSE FESTNER	2017-09-01	                                                                                                                                                                                                                                                          			\N
298		ILSI MARTINEZ	2017-05-07	                                                                                                                                                                                                                                                          			\N
299		INES	2015-11-07	                                                                                                                                                                                                                                                          			\N
300		INES LATERZA	2015-11-07	                                                                                                                                                                                                                                                          	0976355661		\N
301		INGRID GONZALEZ	1991-04-15	                                                                                                                                                                                                                                                          	0986436324		\N
302		IRENE DE AGUILERA	2016-10-01	                                                                                                                                                                                                                                                          	0976941724		\N
303		IRINA	2016-12-04	                                                                                                                                                                                                                                                          			\N
304		IRIS	2015-12-30	                                                                                                                                                                                                                                                          			\N
305		ISABEL	2017-02-05	                                                                                                                                                                                                                                                          			\N
306		ISABEL BARRIOS	2018-01-05	                                                                                                                                                                                                                                                          			\N
307		ISIS GONZALEZ	1988-01-07	                                                                                                                                                                                                                                                          		ichiquel@gmail.com	\N
308		ISIS RIVEROS	2017-01-08	                                                                                                                                                                                                                                                          			\N
309		JANETTE CORREA	2015-09-15	                                                                                                                                                                                                                                                          	0984683001		\N
310		JANIRA	2016-03-01	                                                                                                                                                                                                                                                          			\N
311		JAVIER	1999-12-25	                                                                                                                                                                                                                                                          			\N
312	l	JAVIER LOPEZ	2016-10-14	                                                                                                                                                                                                                                                          			\N
313		JAZMIN	2015-12-22	                                                                                                                                                                                                                                                          			\N
314		JAZMIN ZORRILLA	2016-10-02	                                                                                                                                                                                                                                                          			\N
315		JENNIFER	2017-06-04	                                                                                                                                                                                                                                                          			\N
316		JESSICA	2015-11-30	                                                                                                                                                                                                                                                          			\N
317		JESSICA CENTURION	2017-02-04	                                                                                                                                                                                                                                                          			\N
318		JESSICA VARGAS	2016-10-02	                                                                                                                                                                                                                                                          			\N
319		JOHANA	2015-12-08	                                                                                                                                                                                                                                                          			\N
320		JOHANA PERALTA	2016-06-04	                                                                                                                                                                                                                                                          			\N
321		JORGE ESCOBAR	2015-11-14	                                                                                                                                                                                                                                                          			\N
322		JORGELINA	1988-04-15	                                                                                                                                                                                                                                                          			\N
323		JORGELINA DE AMARILLA	2016-12-04	                                                                                                                                                                                                                                                          			\N
324		JOSEFA	1998-04-16	                                                                                                                                                                                                                                                          			\N
325		JOSEFINA	2017-10-07	                                                                                                                                                                                                                                                          			\N
326		JUAN BILBAO	1978-12-15	                                                                                                                                                                                                                                                          			\N
327		JUAN CAPDEVILLA	2016-09-02	                                                                                                                                                                                                                                                          			\N
328		JUANA	2015-12-30	                                                                                                                                                                                                                                                          			\N
329		JUANA DE ALVAREZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
330		JUANA ELISA GAONA	1990-06-25	                                                                                                                                                                                                                                                          	0973382114		\N
331		JUDITH	2017-03-05	                                                                                                                                                                                                                                                          			\N
332		JULIA	1987-02-15	                                                                                                                                                                                                                                                          			\N
333		JUSTINA ALVISO	1991-02-12	                                                                                                                                                                                                                                                          	0981978001		\N
334		JUSTINAA	2016-01-09	                                                                                                                                                                                                                                                          			\N
335		KAREN	1898-02-04	                                                                                                                                                                                                                                                          			\N
336		KAREN AYALA	2016-10-02	                                                                                                                                                                                                                                                          			\N
337		KAREN CUBILLA	2015-11-05	                                                                                                                                                                                                                                                          			\N
338		KAREN LEDESMA	2015-01-19	                                                                                                                                                                                                                                                          	0994205805		\N
339		KAREN NARDELLI	2016-11-06	                                                                                                                                                                                                                                                          			\N
340		KAREN NUNHEZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
341		KAREN REGUERA	2016-09-06	                                                                                                                                                                                                                                                          			\N
342		KARINA	2015-12-16	                                                                                                                                                                                                                                                          			\N
343		KARINA CARDOZO	1985-10-22	                                                                                                                                                                                                                                                          			\N
344		KARINA OVANDO	2016-11-06	                                                                                                                                                                                                                                                          			\N
345		KARINA SOSA	2016-12-04	                                                                                                                                                                                                                                                          			\N
346		KATERIN PEREIRA	1994-01-15	                                                                                                                                                                                                                                                          	0986313210		\N
347		KATERINE	1889-08-15	                                                                                                                                                                                                                                                          			\N
348		KATHERIN	2015-12-21	                                                                                                                                                                                                                                                          			\N
349		KATI	1565-05-15	                                                                                                                                                                                                                                                          			\N
350		KATI PACUA	2016-07-07	                                                                                                                                                                                                                                                          			\N
351		KIKA	1990-01-01	                                                                                                                                                                                                                                                          			\N
352		LARISSA	2016-09-02	                                                                                                                                                                                                                                                          			\N
353		LAURA	2016-08-12	                                                                                                                                                                                                                                                          			\N
354		LAURA CUEVAS	2017-02-11	                                                                                                                                                                                                                                                          			\N
355		LAURA DE PAOLI	2015-11-06	                                                                                                                                                                                                                                                          			\N
356		LAURA DIAZ	1990-06-14	                                                                                                                                                                                                                                                          	0971340329		\N
357		LAURA OJEDA	0016-06-06	                                                                                                                                                                                                                                                          			\N
358		LAURA OVELAR	2016-06-01	                                                                                                                                                                                                                                                          	0976186291		\N
359		LAURA PAREDEZ	2016-10-02	                                                                                                                                                                                                                                                          			\N
360		LAURA RIVAS	2015-03-30	                                                                                                                                                                                                                                                          	0985961172		\N
361		LAURA(REVISTA)	2016-07-01	                                                                                                                                                                                                                                                          			\N
362		LEDY MARTINEZ	2015-11-27	                                                                                                                                                                                                                                                          			\N
363		LEIDY	2015-11-27	                                                                                                                                                                                                                                                          			\N
364	779912-8	LEILA TORRES	2016-10-06	                                                                                                                                                                                                                                                          			\N
365		LENA MARTINEZ	2017-02-05	                                                                                                                                                                                                                                                          			\N
366		LENY	2017-01-08	                                                                                                                                                                                                                                                          			\N
367		LEONARDA ALMEIDA	2016-07-02	                                                                                                                                                                                                                                                          			\N
368		LESLY	2017-12-21	                                                                                                                                                                                                                                                          			\N
369		LETICIA	2015-03-30	                                                                                                                                                                                                                                                          			\N
370		LETICIA DENIS	2016-10-01	                                                                                                                                                                                                                                                          			\N
371		LETICIA DUARTE	2016-01-31	                                                                                                                                                                                                                                                          	0982762763		\N
372		LETICIA ESCOBAR	2016-12-04	                                                                                                                                                                                                                                                          			\N
373		LIA	2015-12-14	                                                                                                                                                                                                                                                          			\N
374		LIBRY GONZALEZ	2016-12-10	                                                                                                                                                                                                                                                          			\N
375		LICHA	2016-11-06	                                                                                                                                                                                                                                                          			\N
376		LIDIA	2016-05-13	                                                                                                                                                                                                                                                          			\N
377		LIDIA BENEGA	2016-06-10	                                                                                                                                                                                                                                                          			\N
378		LIDUBINA SANTACRUZ	1998-04-16	                                                                                                                                                                                                                                                          	292654		\N
379		LILI ARANA	2016-12-04	                                                                                                                                                                                                                                                          			\N
380		LILI CORONEL	1959-04-14	                                                                                                                                                                                                                                                          	0982153621		\N
381		LILI SCHNEIDER	2015-11-07	                                                                                                                                                                                                                                                          			\N
382		LILIAN MILLAN	2016-12-04	                                                                                                                                                                                                                                                          			\N
383		LILIAN OJEDA	2016-12-11	                                                                                                                                                                                                                                                          			\N
384		LILIAN RIVEROS	2016-12-11	                                                                                                                                                                                                                                                          			\N
385		LILIAN ROJAS	2015-11-20	                                                                                                                                                                                                                                                          			\N
386		LILIAN TORRES	2016-07-13	                                                                                                                                                                                                                                                          	0981206896		\N
387		LILIAN	2015-11-20	                                                                                                                                                                                                                                                          			\N
388		LILIANA	2016-05-20	                                                                                                                                                                                                                                                          			\N
389		LILIANA ARMOA	2016-11-06	                                                                                                                                                                                                                                                          			\N
390		LILIANA RIVEROS	2016-12-04	                                                                                                                                                                                                                                                          			\N
391		LIMPIA DE PINEDO	2016-12-04	                                                                                                                                                                                                                                                          			\N
392		LINA FERNANDEZ	2015-11-24	                                                                                                                                                                                                                                                          			\N
393		LIRICE PORTILLO	2015-12-21	                                                                                                                                                                                                                                                          	0971741656		\N
394		LISSA GALEANO	2016-10-01	                                                                                                                                                                                                                                                          			\N
395		LIZ	2016-01-07	                                                                                                                                                                                                                                                          			\N
396		LIZ ARTIGA	2016-10-09	                                                                                                                                                                                                                                                          			\N
397		LIZ AVENTE	2017-06-05	                                                                                                                                                                                                                                                          			\N
398		LIZ ESTIGARRIBIA	2017-06-04	                                                                                                                                                                                                                                                          			\N
399		LIZ FLORENTIN	1972-07-18	                                                                                                                                                                                                                                                          	0986676751		\N
400		LIZ OJEDA	2016-09-03	                                                                                                                                                                                                                                                          			\N
401		LIZA MONTERO	2016-01-04	                                                                                                                                                                                                                                                          			\N
402		LIZZA	2015-12-14	                                                                                                                                                                                                                                                          			\N
403		LIZZA GALEANO	2016-07-09	                                                                                                                                                                                                                                                          			\N
404		LIZZA PORTILLO	1981-11-09	                                                                                                                                                                                                                                                          	0983250476		\N
405		LIZZI	1985-07-30	                                                                                                                                                                                                                                                          			\N
406		LOLIII	2016-01-18	                                                                                                                                                                                                                                                          			\N
407		LORENA	2015-12-08	                                                                                                                                                                                                                                                          			\N
408		LORENA DAVALOS	2016-10-01	                                                                                                                                                                                                                                                          			\N
409		LORENA FRANCO	2016-02-08	                                                                                                                                                                                                                                                          	0985215742		\N
410		LORENA RIVEROS	2016-11-06	                                                                                                                                                                                                                                                          			\N
411		LORENZA	2017-09-02	                                                                                                                                                                                                                                                          			\N
412		LOURDES	1998-04-16	                                                                                                                                                                                                                                                          			\N
413		LOURDES AMARILLA	0015-06-07	                                                                                                                                                                                                                                                          			\N
414		LOURDES ORTIZ	2015-08-26	                                                                                                                                                                                                                                                          	0994563513		\N
415		LOURDES PANIAGUA	2016-06-09	                                                                                                                                                                                                                                                          			\N
416		LOURDES RUIZ DIAZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
417		LOURDES SOLIS	1971-12-26	                                                                                                                                                                                                                                                          	0981818773		\N
418		LUCAS LOPEZ	1998-12-16	                                                                                                                                                                                                                                                          			\N
419		LUCAS ROSSI	2016-04-02	                                                                                                                                                                                                                                                          			\N
420		LUCIA	2016-01-05	                                                                                                                                                                                                                                                          			\N
421		LUCIANA	2015-12-26	                                                                                                                                                                                                                                                          			\N
422		LUCIO	2015-12-24	                                                                                                                                                                                                                                                          			\N
423		LUISA	2017-01-01	                                                                                                                                                                                                                                                          			\N
424		LUJAN	2015-12-11	                                                                                                                                                                                                                                                          			\N
425		LULA	2015-12-04	                                                                                                                                                                                                                                                          			\N
426		LUZ	2015-11-05	                                                                                                                                                                                                                                                          			\N
427		LUZ AQUINO	1983-11-06	                                                                                                                                                                                                                                                          	0981100619		\N
428		LUZ BENITEZ	2015-01-23	                                                                                                                                                                                                                                                          	0982980155		\N
429		LUZ CARDOZO	2015-11-04	                                                                                                                                                                                                                                                          			\N
430		LUZ LEGUIZAMON	2015-11-20	                                                                                                                                                                                                                                                          			\N
431		MABEL	2015-11-30	                                                                                                                                                                                                                                                          	0981740047		\N
432		MABEL CASTRO	2017-02-05	                                                                                                                                                                                                                                                          			\N
433		MABEL URAN	1983-08-03	                                                                                                                                                                                                                                                          	0981995898		\N
434		MACARENA LOZADA	2015-11-23	                                                                                                                                                                                                                                                          	0982853965		\N
435		MAGALI RIQUELME	2015-11-13	                                                                                                                                                                                                                                                          	0994281407		\N
436		MAGALI.	2015-03-30	                                                                                                                                                                                                                                                          			\N
437		MAGDALENA	2017-04-09	                                                                                                                                                                                                                                                          			\N
438		MAGGI	1998-04-16	                                                                                                                                                                                                                                                          			\N
439		MAIA	2015-12-14	                                                                                                                                                                                                                                                          			\N
440		MAIRA	2016-12-04	                                                                                                                                                                                                                                                          			\N
441		MALE GONZALEZ	0024-02-07	                                                                                                                                                                                                                                                          			\N
442		MALENI	2016-10-01	                                                                                                                                                                                                                                                          			\N
443		MALENI MARINHO 	2016-11-06	                                                                                                                                                                                                                                                          			\N
444		MALU	1978-04-14	                                                                                                                                                                                                                                                          			\N
445		MAMACHA GONZALE	2015-12-04	                                                                                                                                                                                                                                                          	0984768567		\N
446		MANUEL	2016-11-06	                                                                                                                                                                                                                                                          			\N
447		MARA CARDOZO	2016-12-04	                                                                                                                                                                                                                                                          			\N
448		MARCELA	1987-05-15	                                                                                                                                                                                                                                                          			\N
449		MARCIA	2017-10-08	                                                                                                                                                                                                                                                          			\N
450		MARCOS	2016-10-30	                                                                                                                                                                                                                                                          			\N
451		MARGA	2016-11-06	                                                                                                                                                                                                                                                          			\N
452		MARGARITA RAMOS	2017-06-08	                                                                                                                                                                                                                                                          			\N
453		MARI DE CABANHAS	2016-10-01	                                                                                                                                                                                                                                                          			\N
454		MARIA	2015-12-24	                                                                                                                                                                                                                                                          			\N
455		MARIA AURELIA	1987-09-01	                                                                                                                                                                                                                                                          	0994280327		\N
456		MARIA AYALA	2016-11-06	                                                                                                                                                                                                                                                          			\N
457		MARIA BELEN LEIVA	2016-10-02	                                                                                                                                                                                                                                                          			\N
458		MARIA CACERES	2017-02-05	                                                                                                                                                                                                                                                          			\N
459		MARIA DE ALVAREZ	2018-01-26	                                                                                                                                                                                                                                                          			\N
460		MARIA DEL CARMEN	2015-11-20	                                                                                                                                                                                                                                                          	0981407470		\N
461		MARIA DEL CARMEN ESPINOLA	2015-09-18	                                                                                                                                                                                                                                                          	0981423048		\N
462		MARIA ELENA	2015-11-07	                                                                                                                                                                                                                                                          			\N
463		MARIA ELENA ROLON	1972-07-18	                                                                                                                                                                                                                                                          	0981824154		\N
464		MARIA ELISA	2017-03-05	                                                                                                                                                                                                                                                          			\N
465		MARIA EMILIA	2015-12-04	                                                                                                                                                                                                                                                          			\N
466		MARIA ESTHER	2017-05-26	                                                                                                                                                                                                                                                          			\N
467		MARIA ESTHER COLMAN	1983-01-27	                                                                                                                                                                                                                                                          	0981112581		\N
468		MARIA EUGENIA	2016-01-09	                                                                                                                                                                                                                                                          			\N
469		MARIA FLEITAS	2016-06-02	                                                                                                                                                                                                                                                          			\N
470		MARIA GLORIA	2016-05-13	                                                                                                                                                                                                                                                          			\N
471	1.271.809-2	MARIA GRACIELA SANABRIA	2017-09-01	                                                                                                                                                                                                                                                          			\N
472		MARIA INES	2015-12-21	                                                                                                                                                                                                                                                          			\N
473		MARIA INES LATERZA	2016-10-02	                                                                                                                                                                                                                                                          			\N
474		MARIA JOSE	1998-07-15	                                                                                                                                                                                                                                                          			\N
475		MARIA JOSE DAVALOS	1984-11-30	                                                                                                                                                                                                                                                          	0981191311		\N
476		MARIA JOSE ISASI	2015-11-05	                                                                                                                                                                                                                                                          			\N
477		MARIA JOSE MUNHOZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
478		MARIA JOSE ZORZ	2016-08-10	                                                                                                                                                                                                                                                          			\N
479		MARIA JULIA	2016-11-06	                                                                                                                                                                                                                                                          			\N
480		MARIA LAURA	2016-07-02	                                                                                                                                                                                                                                                          			\N
481		MARIA LAURA DEL PUERTO	2016-07-01	                                                                                                                                                                                                                                                          			\N
482		MARIA LIZ	1971-05-19	                                                                                                                                                                                                                                                          			\N
483		MARIA LUISA	2015-11-14	                                                                                                                                                                                                                                                          			\N
484		MARIA ORTIGOZA	2016-11-05	                                                                                                                                                                                                                                                          			\N
485		MARIA ROSA	2016-12-04	                                                                                                                                                                                                                                                          			\N
486		MARIA TERESA SILVA	2015-11-12	                                                                                                                                                                                                                                                          			\N
487		MARIA VALLEJOS	2017-03-05	                                                                                                                                                                                                                                                          			\N
488		MARIAM CACERES	1991-09-10	                                                                                                                                                                                                                                                          	0981764832		\N
489		MARIAN	2016-01-18	                                                                                                                                                                                                                                                          			\N
490		MARIANA	2016-10-01	                                                                                                                                                                                                                                                          			\N
491		MARIANA ALVAREZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
492		MARIANELA	2016-10-08	                                                                                                                                                                                                                                                          			\N
493		MARIANGELA BRESANOVICH	2016-10-02	                                                                                                                                                                                                                                                          			\N
494		MARIANITA	1995-05-05	                                                                                                                                                                                                                                                          			\N
495		MARICEL LUGO	2016-12-04	                                                                                                                                                                                                                                                          			\N
496		MARIEL	1978-02-15	                                                                                                                                                                                                                                                          			\N
497		MARIELA	2016-06-04	                                                                                                                                                                                                                                                          			\N
498		MARIELA ESCOBAR	2017-04-09	                                                                                                                                                                                                                                                          			\N
499		MARINA OVELAR	1981-01-19	                                                                                                                                                                                                                                                          	0981894146		\N
500		MARINKA	2016-01-05	                                                                                                                                                                                                                                                          			\N
501		MARIO GOYA	2016-11-06	                                                                                                                                                                                                                                                          			\N
502		MARIO PAEZ	2015-11-07	                                                                                                                                                                                                                                                          			\N
503		MARIO.	1985-07-30	                                                                                                                                                                                                                                                          			\N
504		MARISA	2015-11-07	                                                                                                                                                                                                                                                          			\N
505		MARISEL LUGO	2016-05-07	                                                                                                                                                                                                                                                          			\N
506		MARISKA	2015-12-11	                                                                                                                                                                                                                                                          			\N
507		MARITE	2015-11-27	                                                                                                                                                                                                                                                          			\N
508		MARIZZA 	2016-10-02	                                                                                                                                                                                                                                                          			\N
509		MARLEN GONZALEZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
510		MARLENA	2016-05-01	                                                                                                                                                                                                                                                          			\N
511		MARLENE	2015-12-09	                                                                                                                                                                                                                                                          			\N
512		MARLY	2015-03-30	                                                                                                                                                                                                                                                          			\N
513		MARTHA	2015-12-08	                                                                                                                                                                                                                                                          			\N
514		MARTIN CARMONA	1981-10-26	                                                                                                                                                                                                                                                          	0981417886		\N
515		MARU GARCIA	2015-12-17	                                                                                                                                                                                                                                                          	0981217118		\N
516		MATIAS	2016-12-04	                                                                                                                                                                                                                                                          			\N
517		MATILDE SOSA	2017-01-01	                                                                                                                                                                                                                                                          			\N
518		MAURA	1998-04-15	                                                                                                                                                                                                                                                          			\N
519		MAYRA	2016-01-08	                                                                                                                                                                                                                                                          			\N
520		MECHE	2016-11-06	                                                                                                                                                                                                                                                          			\N
521		MELENA	2015-12-14	                                                                                                                                                                                                                                                          			\N
522		MELISSA VILLALBA	2017-06-21	                                                                                                                                                                                                                                                          			\N
523		MERCEDES	2016-10-01	                                                                                                                                                                                                                                                          			\N
524		MERCEDES LEON	2015-12-17	                                                                                                                                                                                                                                                          			\N
525		MIA QUINTANA	2016-11-06	                                                                                                                                                                                                                                                          			\N
526		MICAELA	2017-02-05	                                                                                                                                                                                                                                                          			\N
527		MICHEL	2016-10-02	                                                                                                                                                                                                                                                          			\N
528		MILAGROS	2016-12-23	                                                                                                                                                                                                                                                          	0000000		\N
529		MILCIA	2016-07-06	                                                                                                                                                                                                                                                          			\N
530		MIRIAN	2016-09-08	                                                                                                                                                                                                                                                          			\N
531		MIRIAN BOGARIN	2017-01-08	                                                                                                                                                                                                                                                          			\N
532		MIRIAN CABALLERO	2015-11-07	                                                                                                                                                                                                                                                          	0981404499		\N
533		MIRIAN ESPINOZA	1970-04-16	                                                                                                                                                                                                                                                          			\N
534		MIRIAN GIMENEZ	1974-04-15	                                                                                                                                                                                                                                                          			\N
535		MIRIAN RUIZ DIAZ	2016-12-10	                                                                                                                                                                                                                                                          			\N
536		MIRNA ALBAREZ	2016-03-29	                                                                                                                                                                                                                                                          			\N
537		MIRNA AYALA	2017-01-01	                                                                                                                                                                                                                                                          			\N
538		MIRNA DUARTE	2016-12-04	                                                                                                                                                                                                                                                          			\N
539		MIRNA LOPEZ	2017-09-09	                                                                                                                                                                                                                                                          			\N
540		MIRNA NUNHEZ	2017-06-30	                                                                                                                                                                                                                                                          			\N
541		MIRTA RODRIGUEZ	0070-06-13	                                                                                                                                                                                                                                                          	0982651951		\N
542		MITZI	2015-11-23	                                                                                                                                                                                                                                                          			\N
543		MONICA	2015-12-21	                                                                                                                                                                                                                                                          			\N
544		MONICA ACOSTA	2016-12-10	                                                                                                                                                                                                                                                          			\N
545		MONSERRAT LEIVA	1986-10-08	                                                                                                                                                                                                                                                          	0981554990		\N
546	3249086-0	MONSERRAT MERCADO	1984-08-17	                                                                                                                                                                                                                                                          	0981327519		\N
547		NADIA	2015-11-14	                                                                                                                                                                                                                                                          			\N
548		NADIA ALMIRON	2016-07-08	                                                                                                                                                                                                                                                          			\N
549		NADIA GIMENEZ	2016-07-22	                                                                                                                                                                                                                                                          			\N
550		NAHOMI	2015-11-30	                                                                                                                                                                                                                                                          			\N
551		NANCII	2016-01-05	                                                                                                                                                                                                                                                          			\N
552		NANCY	2016-12-04	                                                                                                                                                                                                                                                          			\N
553		NANCY ARANDA	1974-04-09	                                                                                                                                                                                                                                                          	0981773679		\N
554		NANCY GOMEZ	2016-11-13	                                                                                                                                                                                                                                                          			\N
555		NANCY NUNEZ	1984-01-28	                                                                                                                                                                                                                                                          	0981422440	nancynunez33@gmail.com 	\N
556		NANCY ORTIGOZA	2016-12-04	                                                                                                                                                                                                                                                          			\N
557		NANCY PEREIRA	2015-03-30	                                                                                                                                                                                                                                                          			\N
558		NANCY SOSA	2015-12-08	                                                                                                                                                                                                                                                          			\N
559		NATALIA	2016-10-01	                                                                                                                                                                                                                                                          			\N
560		NATALIA CASACCIA	0085-02-17	                                                                                                                                                                                                                                                          	0981797416		\N
561		NATALIA FLEITAS	2016-08-06	                                                                                                                                                                                                                                                          			\N
562		NATALIA MENA	2016-11-06	                                                                                                                                                                                                                                                          			\N
563		NATALIA MORINIGO	1998-04-15	                                                                                                                                                                                                                                                          			\N
564		NATALIA TEIXEIRA	0095-05-10	                                                                                                                                                                                                                                                          	0982831439		\N
565		NATHALIA CASACCIA	2015-02-17	                                                                                                                                                                                                                                                          			\N
566		NATHALIA OCAMPOS	0026-05-08	                                                                                                                                                                                                                                                          			\N
567		NATHALIA OSORIO	2015-03-28	                                                                                                                                                                                                                                                          	0982733336		\N
568		NATHALIA SOSA	2016-11-05	                                                                                                                                                                                                                                                          			\N
569		NATHALIA TEXEIRA	2015-12-08	                                                                                                                                                                                                                                                          			\N
570		NATI FLEITAS	2015-06-10	                                                                                                                                                                                                                                                          	0981270587		\N
571		NATY	2015-12-02	                                                                                                                                                                                                                                                          			\N
572		NAYELY	2017-07-08	                                                                                                                                                                                                                                                          			\N
573		NELLY BOBADILLA	2017-05-14	                                                                                                                                                                                                                                                          			\N
574		NICOL	2015-12-12	                                                                                                                                                                                                                                                          			\N
575		NIDIA	2016-10-01	                                                                                                                                                                                                                                                          			\N
576		NILA VALLEJOS	2016-07-09	                                                                                                                                                                                                                                                          			\N
577		NILCE LUQUE	2015-11-17	                                                                                                                                                                                                                                                          			\N
578		NILDA BOGARIN	2016-09-08	                                                                                                                                                                                                                                                          			\N
579		NILSA	2015-12-30	                                                                                                                                                                                                                                                          			\N
580		NILSA BRITEZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
581		NILSA GARCIA	2015-12-14	                                                                                                                                                                                                                                                          			\N
582		NILSA GONZALEZ	2015-11-04	                                                                                                                                                                                                                                                          			\N
583		NILSA GONZALEZ MANUELITA	0072-04-13	                                                                                                                                                                                                                                                          	0986313210		\N
584		NILSA SOSA	2016-11-06	                                                                                                                                                                                                                                                          			\N
585		NILSA SOTELO	0084-03-24	                                                                                                                                                                                                                                                          			\N
586		NIMIA	2016-09-02	                                                                                                                                                                                                                                                          			\N
587		NIMIA DE LOPEZ	2016-10-02	                                                                                                                                                                                                                                                          			\N
588		NIMIA SANTANDER	0074-04-09	                                                                                                                                                                                                                                                          	0981773679		\N
589		NIMIA VERA	1985-07-17	                                                                                                                                                                                                                                                          	0981522010		\N
590		NINFA	2016-04-12	                                                                                                                                                                                                                                                          			\N
591		NINFA AGUERO	2016-12-10	                                                                                                                                                                                                                                                          			\N
592		NINFA GALEANO	2016-12-04	                                                                                                                                                                                                                                                          			\N
593		NOELIA	2016-10-02	                                                                                                                                                                                                                                                          			\N
594		NOELIA BARRIOCANAL	2016-11-06	                                                                                                                                                                                                                                                          			\N
595		NOEMI	2015-03-30	                                                                                                                                                                                                                                                          			\N
596		NOEMI BAREIRO	2015-12-26	                                                                                                                                                                                                                                                          			\N
597	4693122-8	NORBERTO ORTEGA	1994-08-15	                                                                                                                                                                                                                                                          			\N
598		NORMA	2016-01-07	                                                                                                                                                                                                                                                          			\N
599		NORMA DE CACERES	0023-03-26	                                                                                                                                                                                                                                                          	0981430214	ninsfranc@yahoo.com	\N
600		NORMA DELGADO	2015-02-11	                                                                                                                                                                                                                                                          	0961335379		\N
601	1930374-2	NORMA MEDINA	2016-12-03	                                                                                                                                                                                                                                                          			\N
602		NORMA SILGUERO	2016-05-07	                                                                                                                                                                                                                                                          			\N
603		ODI CACERES	2015-12-16	                                                                                                                                                                                                                                                          			\N
604		ODILIA CACERES	1984-12-26	                                                                                                                                                                                                                                                          	0992288873		\N
605		OLGA	1978-01-24	                                                                                                                                                                                                                                                          			\N
606		OLGA ARIAS	2016-12-04	                                                                                                                                                                                                                                                          			\N
607		OLGA DAVALOS	2015-04-15	                                                                                                                                                                                                                                                          	0981619960		\N
608		OLGA NOGUERA	0015-12-06	                                                                                                                                                                                                                                                          	0985601942	OLGUINNOGUERA@HOTMAIL.COM	\N
609		OLIVIA	2016-07-07	                                                                                                                                                                                                                                                          			\N
610		OSCAR	2015-12-14	                                                                                                                                                                                                                                                          			\N
611	712833-9	OSCAR BENITEZ VARGAS	2017-03-05	                                                                                                                                                                                                                                                          			\N
612		PAMELA	2017-03-05	                                                                                                                                                                                                                                                          			\N
613		PAMELA AGUAYO	1988-05-24	                                                                                                                                                                                                                                                          	0982600752		\N
614		PAMELA BOGADO	1998-04-16	                                                                                                                                                                                                                                                          			\N
615		PAMELA REYES	0014-06-07	                                                                                                                                                                                                                                                          			\N
616		PAOLA	2015-12-29	                                                                                                                                                                                                                                                          			\N
617		PAOLA GAONA	2016-07-02	                                                                                                                                                                                                                                                          			\N
618	1396372-4	PAOLA SANABRIA	2016-06-15	                                                                                                                                                                                                                                                          			\N
619		PATRICIA CALDERINI	2016-07-01	                                                                                                                                                                                                                                                          			\N
620		PATRICIA GODOY	2016-11-12	                                                                                                                                                                                                                                                          			\N
621		PATRICIA GONZALEZ	2016-12-10	                                                                                                                                                                                                                                                          			\N
622		PATRICIA MENDIETA	2015-12-14	                                                                                                                                                                                                                                                          	0981173284		\N
623		PATRICIA MONGELOS	2016-11-06	                                                                                                                                                                                                                                                          			\N
624		PATRICIA ROBLEDO	2017-06-04	                                                                                                                                                                                                                                                          			\N
625		PATRICIA RODRIGUEZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
626		PATRICIA SILVA	2016-12-11	                                                                                                                                                                                                                                                          			\N
627		PATRICIA	2015-11-20	                                                                                                                                                                                                                                                          			\N
628		PAULA CARDOZO	2016-12-04	                                                                                                                                                                                                                                                          			\N
629		PAULINA BRIZUELA	2016-11-06	                                                                                                                                                                                                                                                          			\N
630		PAZ MARTINEZ	2016-12-04	                                                                                                                                                                                                                                                          			\N
631		PERLA BORDON	2016-11-06	                                                                                                                                                                                                                                                          			\N
632		PERLA ZARACHO	2016-07-09	                                                                                                                                                                                                                                                          			\N
633		PETRONA	2016-08-04	                                                                                                                                                                                                                                                          			\N
634		PINEDA	2015-12-12	                                                                                                                                                                                                                                                          			\N
635		PITU	2015-11-23	                                                                                                                                                                                                                                                          			\N
636		PONCHI	2017-12-23	                                                                                                                                                                                                                                                          			\N
637		PRODUCCION TV	2015-12-11	                                                                                                                                                                                                                                                          			\N
638		RAFAELA	2015-11-23	                                                                                                                                                                                                                                                          			\N
639		RAIMUNDA	1987-05-25	                                                                                                                                                                                                                                                          			\N
640		RAMONA	2017-12-30	                                                                                                                                                                                                                                                          			\N
641		RAQUEL	2017-04-02	                                                                                                                                                                                                                                                          			\N
642		RAQUEL ANGULO	2015-12-23	                                                                                                                                                                                                                                                          			\N
643		RAQUEL CENTURION	2016-12-04	                                                                                                                                                                                                                                                          			\N
644		RAQUEL GIMENEZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
645		RAQUEL GONZALEZ	0022-06-07	                                                                                                                                                                                                                                                          			\N
646		REBECA	1998-08-15	                                                                                                                                                                                                                                                          			\N
647	2084322-4	REBECA BORDON	2017-08-05	                                                                                                                                                                                                                                                          			\N
648		REBECA REJALA	2015-03-06	                                                                                                                                                                                                                                                          	0972119184		\N
649		REGINA	2015-12-21	                                                                                                                                                                                                                                                          			\N
650		REINA	2015-12-22	                                                                                                                                                                                                                                                          			\N
651		REVISTA  ZETA	0006-07-10	                                                                                                                                                                                                                                                          			\N
652		RICARDO	2015-11-05	                                                                                                                                                                                                                                                          			\N
653		RICHARD	2016-10-07	                                                                                                                                                                                                                                                          			\N
654		RITA	2017-08-13	                                                                                                                                                                                                                                                          			\N
655		RITA MARTINEZ	2017-05-20	                                                                                                                                                                                                                                                          			\N
656		ROCIO	2015-12-12	                                                                                                                                                                                                                                                          			\N
657		ROCIO FRANCO	2017-03-12	                                                                                                                                                                                                                                                          			\N
658		ROCIO GALEANO	2015-07-06	                                                                                                                                                                                                                                                          	0994136973		\N
659		ROCIO PENAYO	2016-11-06	                                                                                                                                                                                                                                                          			\N
660		ROCIO REJALA	2016-10-08	                                                                                                                                                                                                                                                          			\N
661		RODRIGO SALCEDO	2016-10-02	                                                                                                                                                                                                                                                          			\N
662		ROMINA	2015-12-21	                                                                                                                                                                                                                                                          			\N
663		ROMINA ARCE	2016-11-06	                                                                                                                                                                                                                                                          			\N
664		ROMINA MORENO	2016-10-20	                                                                                                                                                                                                                                                          			\N
665		RONNIE	0097-05-15	                                                                                                                                                                                                                                                          			\N
666		ROSA	2015-12-04	                                                                                                                                                                                                                                                          			\N
667		ROSA BENITEZ	2016-11-06	                                                                                                                                                                                                                                                          			\N
668		ROSA CABRAL	2016-12-04	                                                                                                                                                                                                                                                          			\N
669		ROSA DE FERNANDEZ	2017-01-01	                                                                                                                                                                                                                                                          			\N
670		ROSA MENDOZA	2016-10-01	                                                                                                                                                                                                                                                          			\N
671	1713359-9	ROSA VILLALBA	2018-01-17	                                                                                                                                                                                                                                                          			\N
672		ROSALBA	2017-06-04	                                                                                                                                                                                                                                                          			\N
673		ROSALIA	1978-04-15	                                                                                                                                                                                                                                                          			\N
674		ROSI REBORA	2015-08-28	                                                                                                                                                                                                                                                          	0981910705		\N
675		ROSITA	2016-07-01	                                                                                                                                                                                                                                                          			\N
676		ROSSANA	2016-09-02	                                                                                                                                                                                                                                                          			\N
677		ROSSANA LUGO	2017-02-04	                                                                                                                                                                                                                                                          			\N
678		ROSSANA MORALES	2015-11-26	                                                                                                                                                                                                                                                          			\N
680		ROSSANA OLMEDO	2016-12-11	                                                                                                                                                                                                                                                          			\N
681		ROSSANA PEREIRA	2015-11-11	                                                                                                                                                                                                                                                          			\N
682		ROSSI	2015-12-21	                                                                                                                                                                                                                                                          			\N
683		RUBEN FLEITAS	2016-11-06	                                                                                                                                                                                                                                                          			\N
684		RUBI	2017-10-18	                                                                                                                                                                                                                                                          			\N
685		RUFINA	2015-12-04	                                                                                                                                                                                                                                                          			\N
686		RUMI	2017-06-04	                                                                                                                                                                                                                                                          			\N
687		RUTH	2015-11-12	                                                                                                                                                                                                                                                          			\N
688		RUTH CUQUEJO	2016-12-04	                                                                                                                                                                                                                                                          			\N
689		RUTH DIAZ	1987-08-18	                                                                                                                                                                                                                                                          	0984870077		\N
690		RUTH OVELAR	2015-12-04	                                                                                                                                                                                                                                                          			\N
691		SADI	2017-11-17	                                                                                                                                                                                                                                                          			\N
692		SAHIRA	2015-12-08	                                                                                                                                                                                                                                                          			\N
693		SAIRA	2016-12-04	                                                                                                                                                                                                                                                          			\N
694		SALVADOR GENES	2016-06-03	                                                                                                                                                                                                                                                          			\N
695		SANDRA	2016-01-05	                                                                                                                                                                                                                                                          			\N
696		SANDRA FIGUEREDO	2016-07-01	                                                                                                                                                                                                                                                          			\N
697		SANDRA UDRIZAR	2017-02-04	                                                                                                                                                                                                                                                          			\N
698		SARA	2015-11-17	                                                                                                                                                                                                                                                          			\N
699		SARA CALONGA	2016-11-06	                                                                                                                                                                                                                                                          			\N
700		SARA CUELLAR	2016-11-06	                                                                                                                                                                                                                                                          			\N
701		SARA GOMEZ	2015-10-12	                                                                                                                                                                                                                                                          	0961953747		\N
702		SARA MARTINEZ	2016-10-02	                                                                                                                                                                                                                                                          			\N
703		SARA MONGELOS	2016-08-05	                                                                                                                                                                                                                                                          			\N
705		SARA TABOGADA	2016-10-01	                                                                                                                                                                                                                                                          			\N
706		SELVA ROJAS	2017-06-21	                                                                                                                                                                                                                                                          			\N
707		SHIRLEY	2015-12-11	                                                                                                                                                                                                                                                          			\N
708		SILVANA	2016-10-01	                                                                                                                                                                                                                                                          			\N
709		SILVANA CACERES	2016-12-10	                                                                                                                                                                                                                                                          			\N
710		SILVANA GAONA	1990-07-14	                                                                                                                                                                                                                                                          	0972136227		\N
711		SILVIA	2015-12-16	                                                                                                                                                                                                                                                          			\N
712		SILVIA CRISTALDO	2016-03-05	                                                                                                                                                                                                                                                          	0982415964		\N
713		SILVIA RECALDE	1991-03-02	                                                                                                                                                                                                                                                          	0984245079		\N
714		SILVIA SOTELO	2016-10-01	                                                                                                                                                                                                                                                          			\N
715		SILVIA VILLALBA	2016-11-13	                                                                                                                                                                                                                                                          			\N
716		SILVIA ZARACHO	2015-11-17	                                                                                                                                                                                                                                                          			\N
717	i	SIXTA	2017-05-20	                                                                                                                                                                                                                                                          			\N
718		SOFIA	2015-12-08	                                                                                                                                                                                                                                                          			\N
719		SOFIA DE LLANO	2016-11-06	                                                                                                                                                                                                                                                          			\N
720		SOL	2015-12-21	                                                                                                                                                                                                                                                          			\N
721		SOLANGE	2015-11-13	                                                                                                                                                                                                                                                          			\N
722		SONIA	1889-01-21	                                                                                                                                                                                                                                                          			\N
723		SONIA BOGADO	1986-02-27	                                                                                                                                                                                                                                                          	0981437916		\N
724		SONIA HERMOSA	2016-12-04	                                                                                                                                                                                                                                                          			\N
725		SONIA NAVARRO	2016-11-06	                                                                                                                                                                                                                                                          			\N
726		SONIA VARGAS	2016-01-09	                                                                                                                                                                                                                                                          			\N
727		SONIA VILLALBA	2016-10-05	                                                                                                                                                                                                                                                          			\N
728		SONIA YEGROS	2016-08-05	                                                                                                                                                                                                                                                          			\N
729		SORAIDA	2017-06-04	                                                                                                                                                                                                                                                          			\N
730		SRA ELVIA	2015-12-17	                                                                                                                                                                                                                                                          			\N
731		SRA GRACIELA	2015-11-20	                                                                                                                                                                                                                                                          			\N
732		SRA SARA	2015-11-07	                                                                                                                                                                                                                                                          			\N
733		SRA. KATY	2015-11-03	                                                                                                                                                                                                                                                          			\N
734		SRA. MABEL	1965-08-21	                                                                                                                                                                                                                                                          			\N
735		SRA. MARIANA	2016-05-12	                                                                                                                                                                                                                                                          			\N
736		STEFY	1998-04-15	                                                                                                                                                                                                                                                          			\N
737		SUSANA	2016-10-09	                                                                                                                                                                                                                                                          			\N
738		TADEO	1978-08-15	                                                                                                                                                                                                                                                          			\N
739		TALITA BAREIRO	2017-09-16	                                                                                                                                                                                                                                                          			\N
740		TAMARA	2015-11-30	                                                                                                                                                                                                                                                          			\N
741		TANIA POLETTI	2015-11-23	                                                                                                                                                                                                                                                          			\N
742		TANIA VEGA	1986-09-12	                                                                                                                                                                                                                                                          	0986726354		\N
744		TATIANA	2015-11-13	                                                                                                                                                                                                                                                          			\N
745		TATIANA GONZALEZ	2016-11-13	                                                                                                                                                                                                                                                          			\N
746		TEODOSIA 	1978-09-25	                                                                                                                                                                                                                                                          			\N
747		TERESA GARCETE	2016-12-04	                                                                                                                                                                                                                                                          			\N
748		TERESA SOSA	2015-09-14	                                                                                                                                                                                                                                                          	0981425738		\N
749		THAIS	2017-02-12	                                                                                                                                                                                                                                                          			\N
750		THIAGO	2015-12-21	                                                                                                                                                                                                                                                          			\N
751		TOLI	2015-12-11	                                                                                                                                                                                                                                                          			\N
752		TORIBIA	2015-03-30	                                                                                                                                                                                                                                                          			\N
753		TRIFINA	2015-11-14	                                                                                                                                                                                                                                                          			\N
754		VALENTINA	2016-12-11	                                                                                                                                                                                                                                                          			\N
755		VALERIA	2016-10-09	                                                                                                                                                                                                                                                          			\N
756		VANESSA	1987-01-01	                                                                                                                                                                                                                                                          			\N
757		VANESSA ACOSTA	1983-06-23	                                                                                                                                                                                                                                                          	0984463749		\N
758		VANESSA CENTURION	1991-06-25	                                                                                                                                                                                                                                                          	0971962785		\N
759		VANESSA MARTINEZ	2015-02-13	                                                                                                                                                                                                                                                          	0992531197		\N
760		VERO	2016-12-04	                                                                                                                                                                                                                                                          			\N
761		VERONICA	2017-03-04	                                                                                                                                                                                                                                                          			\N
762		VERONICA MORALES	2016-02-21	                                                                                                                                                                                                                                                          			\N
764		VICENTA	2017-04-02	                                                                                                                                                                                                                                                          			\N
765		VICHE	1995-04-23	                                                                                                                                                                                                                                                          			\N
766		VICKI 	2016-08-04	                                                                                                                                                                                                                                                          			\N
767		VICTOR AYALA	2017-02-05	                                                                                                                                                                                                                                                          			\N
768		VICTORIA	1998-04-16	                                                                                                                                                                                                                                                          			\N
769		VICTORIA OLMEDO	2016-10-05	                                                                                                                                                                                                                                                          			\N
770		VICTORIA SOERENSEN	2017-02-16	                                                                                                                                                                                                                                                          			\N
771	644936-0	VILDA ROJAS	2016-10-01	                                                                                                                                                                                                                                                          			\N
772		VIRGINIA	1984-04-15	                                                                                                                                                                                                                                                          			\N
773		VIVIAN	1990-04-15	                                                                                                                                                                                                                                                          			\N
774		VIVIANA	2016-10-02	                                                                                                                                                                                                                                                          			\N
775		VIVIANA AGUILAR	2015-09-04	                                                                                                                                                                                                                                                          	0981514002		\N
776		VIVIANA VILLALBA	0014-04-17	                                                                                                                                                                                                                                                          			\N
777		VIVIANA VIVEROS	1987-05-24	                                                                                                                                                                                                                                                          			\N
778		WALTRAUD DUCK	2015-11-18	                                                                                                                                                                                                                                                          			\N
779		WILMA	2017-12-15	                                                                                                                                                                                                                                                          			\N
780		YANET	2015-03-30	                                                                                                                                                                                                                                                          			\N
781		YANINA	2016-01-02	                                                                                                                                                                                                                                                          			\N
782		YANINA DA ROSA	2016-10-07	                                                                                                                                                                                                                                                          			\N
783		YANIRA JADIYI	0020-10-08	                                                                                                                                                                                                                                                          			\N
784		YEMIMA	2016-01-07	                                                                                                                                                                                                                                                          			\N
785		YENI CENTURION	2016-11-13	                                                                                                                                                                                                                                                          			\N
786		YOLANDA	1980-01-01	                                                                                                                                                                                                                                                          			\N
787		ZORAIDA	2017-01-01	                                                                                                                                                                                                                                                          			\N
788		ZULLY CASTRO	2016-11-13	                                                                                                                                                                                                                                                          			\N
789		ZULMA VILLALBA	1955-02-15	                                                                                                                                                                                                                                                          			\N
790		ZUNI	2015-12-23	                                                                                                                                                                                                                                                          			\N
791		ZUNILDA AMARILLA	2017-01-01	                                                                                                                                                                                                                                                          			\N
1	4484207-7	FEDERICO LOPEZ	1990-07-02	Asuncion                                                                                                                                                                                                                                                  	0981.789843	fede.jl90@gmail.es	4484207
\.


--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 184
-- Name: cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cliente_id_cliente_seq', 791, true);


--
-- TOC entry 2158 (class 0 OID 16407)
-- Dependencies: 185
-- Data for Name: detalle_boleta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detalle_boleta (id_detalle_boleta, costo, porcentaje, cantidad_porcentaje, id_boleta, id_empleado, id_tratamiento) FROM stdin;
1	35000	14000	40	1	9	5
2	30000	9000	30	1	4	9
4	20000	12000	60	1	10	10
\.


--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 186
-- Name: detalle_boleta_id_detalle_boleta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('detalle_boleta_id_detalle_boleta_seq', 5, true);


--
-- TOC entry 2160 (class 0 OID 16412)
-- Dependencies: 187
-- Data for Name: empleado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY empleado (id_empleado, nombre, ruc, nacimiento, celular, correo, foto, contrasena, pin, estado, id_rol) FROM stdin;
2	JESSICA 	4526084-2	1987-04-15	0981534979				0	A	1
3	ANA		1987-01-26	0985772545				0	A	3
4	GRISELDA		1995-03-12	0972507934				0	A	3
6	WILLIAM		1982-02-20	0981267109				0	A	3
7	SOLE		2018-01-27					0	A	3
8	CAROLINA		2017-05-13					0	A	3
9	GABRIEL		2016-05-12					0	A	3
10	ROMINA		2016-09-01					0	A	3
11	BLANCA		2017-03-01					0	A	3
12	KAREN		2017-12-07					0	A	3
13	PAOLA		2017-06-03					0	A	3
14	CRISTHIAN		2018-01-27					0	A	3
15	LETY		2017-12-07					0	A	3
16	LIZ	\N	\N	0984101971	\N	liz.jpg	\N	1234	A	2
1	OSVALDO	\N	2015-10-02	0981773679	\N	admin.jpg	123456	1504	A	1
5	AIDEE		1994-07-19	0982512653				0	A	3
\.


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 188
-- Name: empleado_id_empleado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empleado_id_empleado_seq', 16, true);


--
-- TOC entry 2162 (class 0 OID 16417)
-- Dependencies: 189
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY rol (id_rol, rol) FROM stdin;
1	superuser
2	admin
3	employed
\.


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 190
-- Name: rol_idrol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rol_idrol_seq', 3, true);


--
-- TOC entry 2164 (class 0 OID 16422)
-- Dependencies: 191
-- Data for Name: tratamiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tratamiento (id_tratamiento, tratamiento, costo, porcentaje) FROM stdin;
1	CORTE DAMA	30000	50
2	CORTE CABALLERO	20000	50
3	LAVADO	15000	30
4	PESTANHA	30000	50
5	PEINADO	35000	40
6	COLOR	100000	40
7	LAVADO KERASYS	25000	30
8	LAVADO LOREAL	35000	30
9	LAVADO CAVIAR	30000	30
10	MANOS	20000	60
11	PIES	30000	60
12	DEPILACION	15000	50
13	LIMPIEZA DE CUTIS	50000	50
14	BANHO A A LA CREMA	25000	30
15	REFLEJOS	120000	40
17	AMPOLLAS	35000	15
18	MAQUILLAJE	60000	50
19	SHOCK DE KERATINA	120000	30
20	ALISADO	250000	30
21	TRATAMIENTO REVLON	120000	30
22	BANHO DE KERATINA	50000	30
23	DECOLORACION	200000	40
24	LAVADO KERASTASE	45000	30
25	BANHO KERASTASE	60000	30
26	LAVADO PRO-KERATIN	35000	30
27	BOTOX	250000	30
28	SHOCK DE KERATINA	120000	30
29	BOTOX	150000	30
30	ALARGUE	50000	30
31	PRODUCTO	200000	15
32	ACEITE LOREAL	35000	15
33	SHOK DE KERATINA	120000	30
34	ALISADO	250000	30
35	DOCUMENTOS COBRADOS	50000	1
36	MASAJE	60000	60
\.


--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 192
-- Name: tratamiento_id_tratamiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tratamiento_id_tratamiento_seq', 36, true);


--
-- TOC entry 2020 (class 2606 OID 16434)
-- Name: boleta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boleta
    ADD CONSTRAINT boleta_pkey PRIMARY KEY (id_boleta);


--
-- TOC entry 2022 (class 2606 OID 16436)
-- Name: cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id_cliente);


--
-- TOC entry 2024 (class 2606 OID 24684)
-- Name: constraint_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT constraint_name UNIQUE (nombre);


--
-- TOC entry 2026 (class 2606 OID 16438)
-- Name: detalle_boleta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_boleta
    ADD CONSTRAINT detalle_boleta_pkey PRIMARY KEY (id_detalle_boleta);


--
-- TOC entry 2028 (class 2606 OID 16440)
-- Name: empleado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT empleado_pkey PRIMARY KEY (id_empleado);


--
-- TOC entry 2030 (class 2606 OID 16442)
-- Name: nombre_unico; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT nombre_unico UNIQUE (nombre);


--
-- TOC entry 2032 (class 2606 OID 16444)
-- Name: rol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id_rol);


--
-- TOC entry 2034 (class 2606 OID 16446)
-- Name: tratamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tratamiento
    ADD CONSTRAINT tratamiento_pkey PRIMARY KEY (id_tratamiento);


--
-- TOC entry 2035 (class 2606 OID 16447)
-- Name: boleta_id_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY boleta
    ADD CONSTRAINT boleta_id_cliente_fkey FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente);


--
-- TOC entry 2036 (class 2606 OID 16452)
-- Name: detalle_boleta_id_boleta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_boleta
    ADD CONSTRAINT detalle_boleta_id_boleta_fkey FOREIGN KEY (id_boleta) REFERENCES boleta(id_boleta);


--
-- TOC entry 2037 (class 2606 OID 16457)
-- Name: detalle_boleta_id_empleado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_boleta
    ADD CONSTRAINT detalle_boleta_id_empleado_fkey FOREIGN KEY (id_empleado) REFERENCES empleado(id_empleado);


--
-- TOC entry 2038 (class 2606 OID 16462)
-- Name: detalle_boleta_id_tratamiento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_boleta
    ADD CONSTRAINT detalle_boleta_id_tratamiento_fkey FOREIGN KEY (id_tratamiento) REFERENCES tratamiento(id_tratamiento);


--
-- TOC entry 2039 (class 2606 OID 16467)
-- Name: empleado_idrol_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT empleado_idrol_fkey FOREIGN KEY (id_rol) REFERENCES rol(id_rol);


--
-- TOC entry 2172 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-03-15 14:31:17

--
-- PostgreSQL database dump complete
--

